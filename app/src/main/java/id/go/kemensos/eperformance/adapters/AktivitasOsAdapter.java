package id.go.kemensos.eperformance.adapters;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdView;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import id.go.kemensos.eperformance.R;
import id.go.kemensos.eperformance.activities.ZoomImage;
import id.go.kemensos.eperformance.data.AktivitasOsModel;

import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

public class AktivitasOsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int TYPE_HEADER = 2;
    private static final int TYPE_ITEM = 1;
    private Activity mActivity;
    private List<AktivitasOsModel> mItems;
    private LayoutInflater mInflater;
    private SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy", Locale.US);

    public AktivitasOsAdapter(Activity mActivity, List<AktivitasOsModel> mItems) {
        this.mActivity = mActivity;
        mInflater = LayoutInflater.from(mActivity);
        this.mItems = mItems;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return TYPE_HEADER;
        } else {
            return TYPE_ITEM;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_HEADER) {
            View view = mInflater.inflate(R.layout.aktivitas_header, parent, false);
            HeaderViewHolder holder = new HeaderViewHolder(view);
            return holder;
        } else {
            View view = mInflater.inflate(R.layout.aktivitas_list_item, parent, false);
            FindHolder holder = new FindHolder(view);
            return holder;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof FindHolder) {

            AktivitasOsModel item = mItems.get(position - 1);
            FindHolder findHolder = (FindHolder) holder;
            if (item.getAktivitasNama() != null) {
                findHolder.username.setText(item.getAktivitasNama());
            } else {
                findHolder.username.setText("-");
            }
            if (item.getKegiatanNama() != null) {
                findHolder.keg.setText(item.getKegiatanNama());
            } else {
                findHolder.keg.setText("-");
            }
            if (item.getCatatan() != null) {
                String sHtml = item.getCatatan();
                Spanned spanned = Html.fromHtml(sHtml);
                findHolder.status.setText("Catatan: \n" + spanned);
            } else {
                findHolder.status.setText("-");
            }
            if (item.getLokasiAlamat() != null) {
                findHolder.lokasi.setText(item.getLokasiAlamat());
            } else {
                findHolder.lokasi.setText("-");
            }
            if (item.getAktivitasBeban() != 0) {
                findHolder.beban.setText(item.getAktivitasBeban() + " menit (" + item.getJenis() + ")");
            } else {
                findHolder.beban.setText("-");
            }

            if(item.getFile().size() > 0) {
                findHolder.newLayout.removeAllViews();
                for (int i = 0; i < item.getFile().size(); i++) {
                    final String[] data = item.getFile().get(i).split("\\|");
                    LinearLayout layout = new LinearLayout(mActivity);
                    layout.setOrientation(LinearLayout.HORIZONTAL);
                    if (data[2].toString().contains("image/")) {
                        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                                WRAP_CONTENT, 350);
                        layoutParams.setMargins(0, 10, 0, 10);
                        layoutParams.gravity = Gravity.CENTER;

                        TextView no = new TextView(mActivity);
                        ImageView gambar = new ImageView(mActivity);

                        no.setLayoutParams(new LinearLayout.LayoutParams(
                                WRAP_CONTENT,
                                WRAP_CONTENT));
                        no.setText("" + (i + 1) + ". ");
                        String test = "https://forums.androidcentral.com/images/forum_icons/200.png";
                        Picasso.get().load(data[3]).into(gambar);
                        gambar.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent FindIntent = new Intent(mActivity, ZoomImage.class);
                                FindIntent.putExtra("url", data[3]);
                                mActivity.startActivity(FindIntent);
                            }
                        });
                        if(item.getFile().size() > 1){
                            layout.addView(no);
                        }
                        layout.addView(gambar,layoutParams);
                    } else {
                        //String filename = getFileNameFromURL(data[3]);
                        TextView no = new TextView(mActivity);
                        TextView text = new TextView(mActivity);
                        no.setLayoutParams(new LinearLayout.LayoutParams(
                                LinearLayout.LayoutParams.WRAP_CONTENT,
                                LinearLayout.LayoutParams.WRAP_CONTENT));
                        no.setText("" + (i + 1) + ". ");
                        text.setLayoutParams(new LinearLayout.LayoutParams(
                                LinearLayout.LayoutParams.MATCH_PARENT,
                                LinearLayout.LayoutParams.MATCH_PARENT));
                        text.setText(data[1]);
                        if(item.getFile().size() > 1){
                            layout.addView(no);
                        }
                        layout.addView(text);
                    }
                    findHolder.newLayout.addView(layout);
                }
            } else {
                findHolder.newLayout.removeAllViews();
                TextView no = new TextView(mActivity);
                no.setLayoutParams(new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT));
                no.setText(" -");
                findHolder.newLayout.addView(no);
            }
            //findHolder.tanggal.setText(item.getTanggal().toString());
            String stgl = "";
            try {
                stgl = sdf.format(item.getTanggal());
            }
            catch (Exception e) {}
            findHolder.tanggal.setText(stgl);

            if (item.getCatatanAtasan() != null && item.getCatatanAtasan() != "" && !item.getCatatanAtasan().isEmpty()) {
                findHolder.komentar.setText(item.getCatatanAtasan());
            } else {
                findHolder.komentar_lbl.setVisibility(View.GONE);
                findHolder.komentar.setVisibility(View.GONE);
            }
            /*Picasso.with(mActivity.getApplicationContext())
                    .load(item.getAvatar())
                    .transform(new CropSquareTransformation())
                    .error(R.drawable.image_holder)
                    .placeholder(R.drawable.image_holder)
                    .into(findHolder.picture);*/
            findHolder.nomor.setText(String.valueOf(item.getNomor()));
        } else if (holder instanceof HeaderViewHolder) {
            HeaderViewHolder headerHolder = (HeaderViewHolder) holder;
            {
                headerHolder.mAdView.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public int getItemCount() {
        if (mItems != null) {
            return mItems.size() + 1;
        } else {
            return 0;
        }
    }

    public void setItems(List<AktivitasOsModel> itemModels) {
        this.mItems = itemModels;
        notifyDataSetChanged();
    }
    public List<AktivitasOsModel> getItems() {
        return this.mItems;
    }

    public class FindHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView username, status, tanggal, nomor, keg, komentar_lbl, komentar, lokasi, beban;
        LinearLayout newLayout;

        public FindHolder(View v) {
            super(v);
            username = (TextView) v.findViewById(R.id.username);
            beban = (TextView) v.findViewById(R.id.beban);
            status = (TextView) v.findViewById(R.id.status);
            tanggal = (TextView) v.findViewById(R.id.tanggal);
            nomor = (TextView) v.findViewById(R.id.nomor);
            keg = (TextView) v.findViewById(R.id.keg);
            komentar_lbl = (TextView) v.findViewById(R.id.komentar_lbl);
            komentar = (TextView) v.findViewById(R.id.komentar);
            lokasi = (TextView) v.findViewById(R.id.lokasi);
            newLayout = (LinearLayout) v.findViewById(R.id.pendukung);
            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

        }
    }

    public class HeaderViewHolder extends RecyclerView.ViewHolder {
        public AdView mAdView;

        public HeaderViewHolder(View itemView) {
            super(itemView);
            mAdView = (AdView) itemView.findViewById(R.id.adView);
        }
    }
}
