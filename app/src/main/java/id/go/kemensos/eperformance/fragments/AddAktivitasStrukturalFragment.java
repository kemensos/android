package id.go.kemensos.eperformance.fragments;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Fragment;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import id.go.kemensos.eperformance.FileUtils;
import id.go.kemensos.eperformance.GPSTracker;
import id.go.kemensos.eperformance.ImagePicker;
import id.go.kemensos.eperformance.PinchZoomImageView;
import id.go.kemensos.eperformance.R;
import id.go.kemensos.eperformance.api.APIService;
import id.go.kemensos.eperformance.api.GlobalAPI;
import id.go.kemensos.eperformance.data.MasterAktivitasModel;
import id.go.kemensos.eperformance.data.PenugasanKegiatanModel;
import id.go.kemensos.eperformance.data.St;
import id.go.kemensos.eperformance.helpers.CustomAlertAdapter;
import id.go.kemensos.eperformance.helpers.M;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.MultipartTypedOutput;
import retrofit.mime.TypedFile;
import retrofit.mime.TypedString;

public class AddAktivitasStrukturalFragment extends Fragment implements OnClickListener {
    public View mView;
    private Button btnSave;
    EditText tanggal, aktivitas, kegiatan, catatan, satuan_output, jumlah_output, menit_waktu_pengerjaan;
    RadioGroup rg;
    String aktivitas_string, kegiatan_id;
    int jenis_waktu_pengerjaan = 0;
    AlertDialog.Builder builderKeg, builderAkt;
    private SimpleDateFormat dateFormatter, ymdFormatter;
    private DatePickerDialog tanggalDlg;
    private SimpleDateFormat bulanFormatter;

    List<PenugasanKegiatanModel> kegiatans;
    List<MasterAktivitasModel> master_aktivitas;
    String jenis = "0", tanggal_ymd;

    private static final int REQUEST_IMAGE_PICK = 0;
    private ImageButton btnPilihGambar;
    String lokasi_koordinat, lokasi_alamat;
    //final List<String> realPath = new ArrayList<>();
    ContentValues values;
    Uri imageUri;
    //ListView lView;
    //ImageAdapter lAdapter;
    String imagePath = null;
    ImageView lampiran;
    GPSTracker gps;
    //Location lokasi = null;
    TextView lokasilbl, lblPilihGambar, lblPercent, jmlPercent;
    ImageButton btnSinkron;
    //GPSHelper gh;
    PinchZoomImageView pinchZoomImageView;
    LinearLayout layoutPercent;
    ProgressBar percent;
    int persentase = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mView = inflater.inflate(R.layout.activity_addnew_struktural, container,
                false);

        this.getActivity().setTitle("Entry Aktivitas");

        builderKeg = new AlertDialog.Builder(getActivity());
        builderAkt = new AlertDialog.Builder(getActivity());

        initializeView();

        gps = new GPSTracker(getActivity());

        sinkronLokasi();

        return mView;
    }

    private void getRealisasi(String bulan) {
        GlobalAPI api = APIService.createService(GlobalAPI.class, M.getToken(getActivity()));
        api.get_target_realisasi("getTargetRealisasiAktivitasPerbulan",
                M.getToken(getActivity()),
                St.getInstance().getLogin().getData().getPegawaiId(),
                bulan,
                new Callback<Response>() {
                    @Override
                    public void success(Response resultx, Response response) {
                        //
                        BufferedReader reader = null;
                        StringBuilder sb = new StringBuilder();
                        try {
                            reader = new BufferedReader(new InputStreamReader(resultx.getBody().in()));
                            String line;
                            try {
                                while ((line = reader.readLine()) != null) {
                                    sb.append(line);
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        String res = sb.toString();
                        try {
                            JSONObject obj = new JSONObject(res);
                            JSONObject data = obj.getJSONObject("data");
                            Integer success = data.getInt("success");
                            if (success == 1) {
                                Integer target = data.getInt("target");
                                Integer realisasi = data.getInt("realisasi");
                                if (realisasi >= target) {
                                    persentase = 100;
                                } else {
                                    persentase = (int) (realisasi * 100 / target);
                                }
                                System.out.println("Presentase = " + persentase);
                                percent.setProgress(persentase);
                                Drawable progressDrawable = percent.getProgressDrawable();
                                if (persentase > 50) {
                                    progressDrawable.setColorFilter(Color.GREEN, android.graphics.PorterDuff.Mode.MULTIPLY);
                                } else {
                                    progressDrawable.setColorFilter(Color.YELLOW, android.graphics.PorterDuff.Mode.MULTIPLY);
                                }
                                percent.setProgressDrawable(progressDrawable);
                                jmlPercent.setText(persentase + "%");
                            }
                        } catch (Exception e) {
                            M.T(getActivity(), e.getMessage());
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        M.T(getActivity(), getString(R.string.ServerError));
                        System.out.println("ERR: " + error.getMessage());
                    }
                });
    }

    private void getKegiatan(String bulan) {
        M.showLoadingDialog(getActivity());
        GlobalAPI api = APIService.createService(GlobalAPI.class, M.getToken(getActivity()));
        api.get_kegiatan("getDaftarPenugasanKegiatanPegawaiPerbulan",
                M.getToken(getActivity()),
                St.getInstance().getLogin().getData().getPegawaiId(),
                bulan,
                new Callback<Response>() {
                    @Override
                    public void success(Response resultx, retrofit.client.Response response) {
                        M.hideLoadingDialog();
                        //
                        BufferedReader reader = null;
                        StringBuilder sb = new StringBuilder();
                        try {
                            reader = new BufferedReader(new InputStreamReader(resultx.getBody().in()));
                            String line;
                            try {
                                while ((line = reader.readLine()) != null) {
                                    sb.append(line);
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        String res = sb.toString();
                        try {
                            JSONObject obj = new JSONObject(res);
                            JSONObject data = obj.getJSONObject("data");
                            Integer success = data.getInt("success");
                            kegiatans = new ArrayList<PenugasanKegiatanModel>();
                            if (success == 1) {
                                JSONArray hasils = data.getJSONArray("penugasanKegiatan");
                                for (int i = 0; i < hasils.length(); i++) {
                                    PenugasanKegiatanModel tmp = new PenugasanKegiatanModel();
                                    tmp.setId(hasils.getJSONObject(i).getString("id"));
                                    tmp.setKode(hasils.getJSONObject(i).getString("kode"));
                                    tmp.setNama(hasils.getJSONObject(i).getString("nama"));
                                    tmp.setJenis(hasils.getJSONObject(i).getString("jenis"));
                                    kegiatans.add(tmp);
                                }
                            }
                        } catch (Exception e) {
                            M.T(getActivity(), e.getMessage());
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        M.hideLoadingDialog();
                        M.T(getActivity(), getString(R.string.ServerError));
                        System.out.println("ERR: " + error.getMessage());
                    }
                });
    }

    private void getMasterAktivitas(final String bulan) {
        //M.showLoadingDialog(getActivity());
        GlobalAPI api = APIService.createService(GlobalAPI.class, M.getToken(getActivity()));
        api.get_master_aktivitas_struktural("getDaftarAktivitasStruktural",
                M.getToken(getActivity()),
                new Callback<Response>() {
                    @Override
                    public void success(Response resultx, Response response) {
                        M.hideLoadingDialog();
                        //
                        BufferedReader reader = null;
                        StringBuilder sb = new StringBuilder();
                        try {
                            reader = new BufferedReader(new InputStreamReader(resultx.getBody().in()));
                            String line;
                            try {
                                while ((line = reader.readLine()) != null) {
                                    sb.append(line);
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        String res = sb.toString();
                        try {
                            JSONObject obj = new JSONObject(res);
                            JSONObject data = obj.getJSONObject("data");
                            Integer success = data.getInt("success");
                            master_aktivitas = new ArrayList<MasterAktivitasModel>();
                            if (success == 1) {
                                JSONArray hasils = data.getJSONArray("daftarAktivitas");
                                for (int i = 0; i < hasils.length(); i++) {
                                    MasterAktivitasModel tmp = new MasterAktivitasModel();
                                    tmp.setNama(hasils.getString(i));
                                    master_aktivitas.add(tmp);
                                }
                            }
                            getKegiatan(bulan);
                        } catch (Exception e) {
                            M.T(getActivity(), e.getMessage());
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        M.hideLoadingDialog();
                        M.T(getActivity(), getString(R.string.ServerError));
                        System.out.println("ERR: " + error.getMessage());
                    }
                });
    }

    public void initializeView() {
        layoutPercent = (LinearLayout) getActivity().findViewById(R.id.layout_persentase);
        lblPercent = (TextView) getActivity().findViewById(R.id.lbl_persentase);
        jmlPercent = (TextView) getActivity().findViewById(R.id.total_persentase);
        percent = (ProgressBar) getActivity().findViewById(R.id.persentase);
        layoutPercent.setVisibility(View.VISIBLE);
        Calendar c = Calendar.getInstance();
        String[] monthName={"Januari","Februari","Maret", "April", "Mei", "Juni", "Juli",
                "Agustus", "September", "Oktober", "November",
                "Desember"};
        getRealisasi(String.valueOf((c.get(Calendar.MONTH) + 1)));
        lblPercent.setText("Capaian Aktivitas Bulan " + monthName[c.get(Calendar.MONTH)]);
        btnSave = (Button) mView.findViewById(R.id.btnSave);
        btnPilihGambar = (ImageButton) mView.findViewById(R.id.btnPilihGambar);
        lblPilihGambar = (TextView) mView.findViewById(R.id.lblPilihGambar);
        btnSinkron = (ImageButton) mView.findViewById(R.id.sinkron);
        lokasilbl = (TextView) mView.findViewById(R.id.lokasi_lbl);
        lampiran = (ImageView) mView.findViewById(R.id.gambarPendukung);
//        Zoomy.Builder builder = new Zoomy.Builder(getActivity()).target(lampiran);
//        builder.register();

        aktivitas = (EditText) mView.findViewById(R.id.aktivitas);
        kegiatan = (EditText) mView.findViewById(R.id.kegiatan);
        catatan = (EditText) mView.findViewById(R.id.catatan);
        satuan_output = (EditText) mView.findViewById(R.id.satuanOutput);
        jumlah_output = (EditText) mView.findViewById(R.id.jmlOutput);
        menit_waktu_pengerjaan = (EditText) mView.findViewById(R.id.waktuPengerjaan);

        rg = (RadioGroup) mView.findViewById(R.id.radioGroupAktivitas);
        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.rbJamKerja:
                        // do operations specific to this selection
                        jenis_waktu_pengerjaan = 0;
                        break;
                    case R.id.rbLuarJamKerja:
                        // do operations specific to this selection
                        jenis_waktu_pengerjaan = 1;
                        break;
                }
            }
        });
        tanggal = (EditText) mView.findViewById(R.id.tanggal);
        tanggal.setOnClickListener(this);
        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        ymdFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        bulanFormatter = new SimpleDateFormat("MM", Locale.US);
        Date date = new Date();
        tanggal.setText(dateFormatter.format(date));
        tanggal_ymd = ymdFormatter.format(date);
        Calendar newCalendar = Calendar.getInstance();
        tanggalDlg = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                tanggal.setText(dateFormatter.format(newDate.getTime()));
                tanggal_ymd = ymdFormatter.format(newDate.getTime());
                getMasterAktivitas(bulanFormatter.format(newDate.getTime()).toString());
                //getKegiatan(bulanFormatter.format(newDate.getTime()).toString());
            }
        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));


        aktivitas.setOnClickListener(this);
        kegiatan.setOnClickListener(this);
        btnSave.setOnClickListener(this);
        btnPilihGambar.setOnClickListener(this);
        lblPilihGambar.setOnClickListener(this);
        btnSinkron.setOnClickListener(this);
        lampiran.setOnClickListener(this);

        getMasterAktivitas(bulanFormatter.format(date).toString());
    }

    private void doSimpan() {
        String status = St.getInstance().getAktif_status();
        String id = "";
        M.showLoadingDialog(getActivity());
        GlobalAPI api = APIService.createService(GlobalAPI.class, M.getToken(getActivity()));
        api.aktivitas_simpan(data("doSimpanAktivitasPegawai".toString(), M.getToken(getActivity()).toString(),
                St.getInstance().getLogin().getData().getPegawaiId().toString(), id.toString(), tanggal_ymd.toString(),
                aktivitas_string.toString(), menit_waktu_pengerjaan.getText().toString().trim(),
                jumlah_output.getText().toString().trim(), satuan_output.getText().toString().trim(),
                String.valueOf(jenis_waktu_pengerjaan), kegiatan_id.toString(), jenis.toString(),
                catatan.getText().toString().trim(), lokasi_koordinat.toString(),
                lokasi_alamat.toString(), imagePath),
                new Callback<Response>() {
                    @Override
                    public void success(Response resultx, retrofit.client.Response response) {
                        M.hideLoadingDialog();
                        //
                        BufferedReader reader = null;
                        StringBuilder sb = new StringBuilder();
                        try {
                            reader = new BufferedReader(new InputStreamReader(resultx.getBody().in()));
                            String line;
                            try {
                                while ((line = reader.readLine()) != null) {
                                    sb.append(line);
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        String res = sb.toString();
                        try {
                            JSONObject obj = new JSONObject(res);
                            JSONObject data = obj.getJSONObject("data");
                            Integer success = data.getInt("success");
                            String msg = data.getString("msg");
                            M.T(getActivity(), msg);
                            if (success == 1) {
                                clearData();
                            } else {
                                tanggal.setError("tanggal invalid");
                            }
                        } catch (Exception e) {
                            M.T(getActivity(), e.getMessage());
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        M.hideLoadingDialog();
                        M.T(getActivity(), getString(R.string.ServerError));
                        System.out.println("ERR: " + error.getMessage());
                    }
                });
    }

    private void clearData() {
        tanggal.setError(null);
        aktivitas.setText("");
        aktivitas_string = "";
        menit_waktu_pengerjaan.setText("");
        jumlah_output.setText("");
        satuan_output.setText("");
        kegiatan.setText("");
        kegiatan_id = "";
        catatan.setText("");
//        if(!realPath.isEmpty()){
//            realPath.clear();
//        }
        if (imageUri != null) {
            imageUri = null;
        }
        gps.getLocation();
        sinkronLokasi();
        imagePath = null;
        if (lampiran.getDrawable() != null) {
            lampiran.setImageResource(0);
        }
        //updateImageList();
    }

    private ArrayList<String> array_sort;
    private String ars_aktivitas[];

    @Override
    public void onClick(View v) {
        AlertDialog alertCat;
        List<String> lst = new ArrayList<>();
        final String[] ars;
        if (v.getId() == R.id.btnSave) {
            if (St.getInstance().getLogin().getData().getJabatan().equalsIgnoreCase("Jabatan Belum diset")) {
                Toast.makeText(getActivity(), "Jabatan anda belum disetting, setting jabatan anda terlebih dahulu sebelum melakukan entry aktivitas",
                        Toast.LENGTH_LONG).show();
                return;
            } else if (aktivitas.getText().toString().equalsIgnoreCase("") || aktivitas_string.equalsIgnoreCase("") || aktivitas_string == "") {
                aktivitas.setError("Pilih aktivitas");
                Toast.makeText(getActivity(), "Pilih aktivitas",
                        Toast.LENGTH_LONG).show();
                return;
            } else if (kegiatan.getText().toString().equalsIgnoreCase("")) {
                kegiatan.setError("Pilih kegiatan");
                Toast.makeText(getActivity(), "Pilih kegiatan",
                        Toast.LENGTH_LONG).show();
                return;
            } else if (catatan.getText().toString().trim().equalsIgnoreCase("") || catatan.toString().trim().length() < 15) {
                catatan.setError("Isi catatan minimal 15 karakter");
                Toast.makeText(getActivity(), "Isi catatan minimal 15 karakter",
                        Toast.LENGTH_LONG).show();
                return;
            } else if (menit_waktu_pengerjaan.getText().toString().equalsIgnoreCase("") || Integer.parseInt(menit_waktu_pengerjaan.getText().toString()) < 5) {
                menit_waktu_pengerjaan.setError("Isi waktu pengerjaan minimal 5 menit");
                Toast.makeText(getActivity(), "Isi waktu pengerjaan minimal 5 menit",
                        Toast.LENGTH_LONG).show();
                return;
            } else if (jumlah_output.getText().toString().equalsIgnoreCase("")) {
                jumlah_output.setError("Isi jumlah output");
                Toast.makeText(getActivity(), "Isi jumlah output",
                        Toast.LENGTH_LONG).show();
                return;
            } else if (satuan_output.getText().toString().equalsIgnoreCase("")) {
                satuan_output.setError("Isi satuan output");
                Toast.makeText(getActivity(), "Isi satuan output",
                        Toast.LENGTH_LONG).show();
                return;
            } else if (!gps.canGetLocation()) {
                Toast.makeText(getActivity(), "Gagal mengambil info lokasi, aktifkan GPS terlebih dahulu sebelum entry aktivitas",
                        Toast.LENGTH_LONG).show();
                return;
            } else if (imagePath == null) {
                Toast.makeText(getActivity(), "Unggah data pendukung terlebih dahulu !",
                        Toast.LENGTH_LONG).show();
                return;
            } else if (lokasi_alamat == null || lokasi_alamat.equalsIgnoreCase("")) {
                Toast.makeText(getActivity(), "Tidak dapat menemukan lokasi entry aktivitas, silahkan coba lagi",
                        Toast.LENGTH_LONG).show();
                return;
            }

//            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2){
//                if(gps.isMockLocation(lokasi)){
//                    Toast.makeText(getActivity(), "Terdeteksi adanya penggunaan lokasi palsu pada perangkat anda, silahkan dinonaktifkan terlebih dahulu kemudian restart device anda.",
//                            Toast.LENGTH_LONG).show();
//                    return;
//                }
//            } else {
//                if(gps.isMockLocationEnabled(getActivity())){
//                    Toast.makeText(getActivity(), "Terdeteksi adanya penggunaan lokasi palsu pada perangkat anda, silahkan dinonaktifkan terlebih dahulu kemudian restart device anda.",
//                            Toast.LENGTH_LONG).show();
//                    return;
//                }
//            }

            doSimpan();
        } else if (v.getId() == R.id.tanggal) {
            long now = System.currentTimeMillis() - 1000;
            tanggalDlg.getDatePicker().setMinDate(now - (1000 * 60 * 60 * 24 * 6));
            tanggalDlg.getDatePicker().setMaxDate(new Date().getTime());
            tanggalDlg.setInverseBackgroundForced(true);
            tanggalDlg.show();
        } else if (v.getId() == R.id.aktivitas) {
            if (master_aktivitas == null) {
                aktivitas.setError("Gagal ambil info aktivitas");
                return;
            }
            for (MasterAktivitasModel s : master_aktivitas) {
                lst.add(s.getNama());
            }
            ars = lst.toArray(new String[lst.size()]);
            ars_aktivitas = ars;
            builderAkt.setTitle("Pilih aktivitas");

            do_aktivitas();

        } else if (v.getId() == R.id.kegiatan) {
            if (kegiatans == null) {
                kegiatan.setError("Gagal ambil info kegiatan");
                return;
            }
            for (PenugasanKegiatanModel s : kegiatans) {
                lst.add(s.getNama());
            }
            ars = lst.toArray(new String[lst.size()]);
            builderKeg.setTitle("Pilih kegiatan");
            builderKeg.setItems(ars, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int item) {
                    String str = ars[item];
                    if (str.equalsIgnoreCase("none"))
                        str = "";
                    kegiatan.setText(str);
                    for (PenugasanKegiatanModel s : kegiatans) {
                        if (s.getNama().equalsIgnoreCase(str)) {
                            kegiatan_id = s.getId();
                            jenis = s.getJenis();
                            break;
                        }
                    }
                }
            });
            builderKeg.setInverseBackgroundForced(true);
            alertCat = builderKeg.create();
            alertCat.show();
        } else if (v.getId() == R.id.btnPilihGambar) {
            Intent chooseImageIntent = ImagePicker.getPickImageIntent(getActivity());
            startActivityForResult(chooseImageIntent, REQUEST_IMAGE_PICK);
        } else if (v.getId() == R.id.lblPilihGambar) {
            Intent chooseImageIntent = ImagePicker.getPickImageIntent(getActivity());
            startActivityForResult(chooseImageIntent, REQUEST_IMAGE_PICK);
        } else if (v.getId() == R.id.sinkron) {
            if (gps.isLocationEnabled(getActivity())) {
                gps.getLocation();
                M.showLoadingDialog(getActivity());
                sinkronLokasi();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        M.hideLoadingDialog();
                    }
                }, 1500);
            } else {
                gps.showSettingsAlert();
            }
        } else if (v.getId() == R.id.gambarPendukung) {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
            LayoutInflater factory = LayoutInflater.from(getActivity());
            View mView = factory.inflate(R.layout.dialog_zoom_image, null);
            pinchZoomImageView = (PinchZoomImageView) mView.findViewById(R.id.pinchZoomImageView);
            pinchZoomImageView.setImageUri(imageUri);
            dialogBuilder.setView(mView);
            AlertDialog dialog = dialogBuilder.create();
            dialog.show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {
            case REQUEST_IMAGE_PICK:
                Bitmap bitmap = ImagePicker.getImageFromResult(getActivity(), resultCode, data);
                if (bitmap != null) {
                    lampiran.setImageBitmap(bitmap);
                    File file = ImagePicker.createImageFile(getActivity());
                    imageUri = ImagePicker.getUriFromFile(file, bitmap);
                    imagePath = FileUtils.getPath(getActivity(), imageUri);
                }
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
                break;
        }

    }

    private MultipartTypedOutput data(String action, String token, String idPegawai, String id, String tanggal,
                                      String stringAktivitas, String menitWaktuPengerjaan, String jumlahOutput,
                                      String satuanOutput, String jenisWaktuPengerjaan, String idKegiatan, String jenis,
                                      String catatan, String koordinat, String lokasi, String path) {
        MultipartTypedOutput multipartTypedOutput = new MultipartTypedOutput();
        multipartTypedOutput.addPart("obj[action]", new TypedString(action));
        multipartTypedOutput.addPart("obj[token]", new TypedString(token));
        multipartTypedOutput.addPart("obj[pegawai_id]", new TypedString(idPegawai));
        multipartTypedOutput.addPart("obj[id]", new TypedString(id));
        multipartTypedOutput.addPart("obj[tanggal]", new TypedString(tanggal));
        multipartTypedOutput.addPart("obj[aktivitas_string]", new TypedString(stringAktivitas));
        multipartTypedOutput.addPart("obj[menit_waktu_pengerjaan]", new TypedString(menitWaktuPengerjaan));
        multipartTypedOutput.addPart("obj[jumlah_output]", new TypedString(jumlahOutput));
        multipartTypedOutput.addPart("obj[satuan_output]", new TypedString(satuanOutput));
        multipartTypedOutput.addPart("obj[jenis_waktu_pengerjaan]", new TypedString(jenisWaktuPengerjaan));
        multipartTypedOutput.addPart("obj[kegiatan_id]", new TypedString(idKegiatan));
        multipartTypedOutput.addPart("obj[jenis]", new TypedString(jenis));
        multipartTypedOutput.addPart("obj[catatan]", new TypedString(catatan));
        multipartTypedOutput.addPart("obj[lokasi_koordinat]", new TypedString(koordinat));
        multipartTypedOutput.addPart("obj[lokasi_alamat]", new TypedString(lokasi));
        multipartTypedOutput.addPart("files[]", new TypedFile("image/jpeg", new File(path)));
//        if(path.isEmpty()){
//
//        } else {
//            for (int i = 0; i < path.size(); i++) {
//                multipartTypedOutput.addPart("files[]", new TypedFile("image/jpg", new File(path.get(i))));
//            }
//        }

        return multipartTypedOutput;
    }

    int textlength = 0;
    private AlertDialog myalertDialog = null;

    void do_aktivitas() {
        final EditText editText = new EditText(this.getActivity());
        final ListView listview = new ListView(this.getActivity());
        editText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_action_search, 0, 0, 0);
        array_sort = new ArrayList<String>(Arrays.asList(ars_aktivitas));
        LinearLayout layout = new LinearLayout(this.getActivity());
        layout.setOrientation(LinearLayout.VERTICAL);
        layout.addView(editText);
        layout.addView(listview);
        builderAkt.setView(layout);
        CustomAlertAdapter arrayAdapter = new CustomAlertAdapter(this.getActivity(), array_sort);
        listview.setAdapter(arrayAdapter);
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                //alertCat.dismiss();
                myalertDialog.dismiss();
                //String str = ars_aktivitas[position];
                String str = array_sort.get(position).toString();
                if (str.equalsIgnoreCase("none"))
                    str = "";
                aktivitas.setText(str);
                for (MasterAktivitasModel s : master_aktivitas) {
                    if (s.getNama().equalsIgnoreCase(str)) {
                        aktivitas_string = s.getNama();
                        break;
                    }
                }
            }
        });
        editText.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s,
                                          int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                editText.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                textlength = editText.getText().length();
                array_sort.clear();
                for (int i = 0; i < ars_aktivitas.length; i++) {
                    if (textlength <= ars_aktivitas[i].length()) {

                        if (ars_aktivitas[i].toLowerCase().contains(editText.getText().toString().toLowerCase().trim())) {
                            array_sort.add(ars_aktivitas[i]);
                        }
                    }
                }
                listview.setAdapter(new CustomAlertAdapter(AddAktivitasStrukturalFragment.this.getActivity(), array_sort));
            }
        });
        builderAkt.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                aktivitas.setText(editText.getText().toString());
                aktivitas_string = aktivitas.getText().toString();
            }
        });
        builderAkt.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builderAkt.setInverseBackgroundForced(true);
        myalertDialog = builderAkt.show();
    }

    private void sinkronLokasi() {
        if (gps.canGetLocation()) {
            boolean mockLocation = false;
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                mockLocation = gps.getLocation().isFromMockProvider();
            } else {
                mockLocation = gps.isMockLocationOn(getActivity());
            }
            if (mockLocation) {
                gps.showFakeGpsAlert();
            } else {
                double latitude = gps.getLatitude();
                double longitude = gps.getLongitude();

                Geocoder geocoder;
                List<Address> addresses = null;
                geocoder = new Geocoder(getActivity(), Locale.getDefault());

                try {
                    addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                    if (addresses != null && addresses.size() > 0) {
                        String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                        String city = addresses.get(0).getLocality();
                        String state = addresses.get(0).getAdminArea();
                        String country = addresses.get(0).getCountryName();
                        String postalCode = addresses.get(0).getPostalCode();
                        String knownName = addresses.get(0).getFeatureName();

                        lokasilbl.setText(address);
                        lokasi_koordinat = "" + latitude + ", " + longitude;
                        lokasi_alamat = address;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            gps.showSettingsAlert();
        }
    }

}