package id.go.kemensos.eperformance.activities;

//import android.support.v4.app.Fragment;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import id.go.kemensos.eperformance.R;
import id.go.kemensos.eperformance.fragments.AddAktivitasFragment;
import id.go.kemensos.eperformance.fragments.AddAktivitasKapdAsistenSekdaFragment;
import id.go.kemensos.eperformance.fragments.StafFragment;
import id.go.kemensos.eperformance.api.APIService;
import id.go.kemensos.eperformance.api.GlobalAPI;
import id.go.kemensos.eperformance.data.ResponseModel;
import id.go.kemensos.eperformance.data.St;
import id.go.kemensos.eperformance.fragments.AktivitasFragment;
import id.go.kemensos.eperformance.helpers.AlertDialogManager;
import id.go.kemensos.eperformance.helpers.CropSquareTransformation;
import id.go.kemensos.eperformance.helpers.M;
//import com.google.android.gms.gcm.GoogleCloudMessaging;
//import com.google.android.gms.iid.InstanceID;
import com.scottyab.rootbeer.RootBeer;
import com.squareup.picasso.Picasso;

import id.go.kemensos.eperformance.helpers.Sess;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import me.leolin.shortcutbadger.ShortcutBadger;

public class MainActivity extends AppCompatActivity {
    public DrawerLayout mDrawerLayout;
    public NavigationView mNavigationView;
    private Toolbar toolbar;
    private FragmentManager mFragmentManager;
    private Fragment mFragment = null;
    private Intent mIntent = null;
    String _lastFrm = "home";
    AsyncTask<Void, Void, Void> mRegisterTask;
    AlertDialogManager alert = new AlertDialogManager();
    public static String name;
    public static String email;
    LinearLayout layoutPercent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /*if (M.getToken(this) == null) {
            Intent mIntent = new Intent(this, LoginActivity.class);
            startActivity(mIntent);
            finish();
        } else {
            setContentView(R.layout.activity_main);
            initializeView();
            displayView(R.id.homeItem);
        }*/
        Sess sess = new Sess(this);
        //
        setContentView(R.layout.activity_main);
        initializeView();
        St.getInstance().setMain(this);
        //displayView(R.id.homeItem);

        //updateHeaderView(St.getInstance().getUser()); //updateDrawerHeaderInfo(); //tes
        _lastFrm = "home-reload";


        name = Settings.Secure.getString(getBaseContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        email = "crossdev@yahoo.com";
        //GCMRegistrar.checkDevice(this);
        //GCMRegistrar.checkManifest(this); //saat develop aj
        //sensor sik
        /*registerReceiver(mHandleMessageReceiver, new IntentFilter(
                GCommonUtilities.DISPLAY_MESSAGE_ACTION));
        final String regId = GCMRegistrar.getRegistrationId(this);
        // Check if regid already presents
        if (regId.equals("")) {
            // Registration is not present, register now with GCM
            GCMRegistrar.register(this, SENDER_ID);
        }
        else {
            // Device is already registered on GCM
            if (GCMRegistrar.isRegisteredOnServer(this)) {
                // Skips registration.
                //Toast.makeText(getApplicationContext(), "Already registered with GCM", Toast.LENGTH_LONG).show();
                Toast.makeText(getApplicationContext(), "registered...", Toast.LENGTH_LONG).show();
                sess.simpen("reqid", GCMRegistrar.getRegistrationId(getApplicationContext()) );
            }
            else {
                // Try to register again, but not in the UI thread.
                // It's also necessary to cancel the thread onDestroy(),
                // hence the use of AsyncTask instead of a raw thread.
                final Context context = this;
                mRegisterTask = new AsyncTask<Void, Void, Void>() {

                    @Override
                    protected Void doInBackground(Void... params) {
                        // Register on our server
                        // On server creates a new user
                        Sess sess = new Sess(MainActivity.this);
                        sess.simpen("reqid", regId);
                        //GServerUtilities.register(context, name, email, regId, Integer.parseInt(St.getInstance().getLogin().getData().getPegawaiId()));
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void result) {
                        mRegisterTask = null;
                    }

                };
                mRegisterTask.execute(null, null, null);
            }
        }*/
//        final String regId = GCMRegistrar.getRegistrationId(this);
//        // Check if regid already presents
//        if (regId.equals("")) {
//            // Registration is not present, register now with GCM
//            GCMRegistrar.register(this, SENDER_ID);
//        }

        Intent fromEdit = getIntent();
        Bundle b = fromEdit.getExtras();
        if(b!=null)
        {
            if(b.get("edit") == "success") {
                St.getInstance().setAktif_status("0");
                switchContent(new AktivitasFragment());
                mNavigationView.getMenu().findItem(R.id.menuAktivitasBaru).setChecked(true);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        RootBeer rootBeer = new RootBeer(MainActivity.this);
        if (rootBeer.isRooted()) {
            //we found indication of root
            showAlertDialogAndExitApp("Terdeteksi device Rooted, nonaktifkan kemudian restart device.");
        }

        mNavigationView.getMenu().findItem(R.id.menuHomeItem).setVisible(false);
        if(St.getInstance().getLogin().getData()!=null) {
            if (St.getInstance().getLogin().getData().isStaf() == true)
                mNavigationView.getMenu().findItem(R.id.menuAktivitasBawahan).setVisible(false);
            Log.i(this.getClass().getSimpleName(), "levelStruktural="+St.getInstance().getLogin().getData().getLevelStruktural());
//            if (St.getInstance().getLogin().getData().getLevelStruktural()==1)
//                mNavigationView.getMenu().findItem(R.id.menuAktivitasAdd).setVisible(false);
        }
        //-- binprog itu level ke-3

        M.L("MainActivity resume _lastFrm:" + _lastFrm);
        /*if (_lastFrm.equalsIgnoreCase("add")) displayView(R.id.menuAktivitasAdd);
        else if (_lastFrm.equalsIgnoreCase("sah")) displayView(R.id.menuAktivitasTelahDisahkan);
        else if (_lastFrm.equalsIgnoreCase("revisi")) displayView(R.id.menuAktivitasRevisi);
        else if (_lastFrm.equalsIgnoreCase("ditolak")) displayView(R.id.menuAktivitasDitolak);
        else displayView(R.id.menuAktivitasAdd);*/
    }

    private void showAlertDialogAndExitApp(String message) {
        android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(MainActivity.this);

        alertDialog.setTitle("Peringatan");
        alertDialog.setIcon(R.drawable.icon_warning);
        alertDialog.setMessage(message);

        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(android.provider.Settings.ACTION_SETTINGS);
                startActivity(intent);
            }
        });

        alertDialog.setCancelable(false);
        alertDialog.show();
    }

    public void setLastFrm(String str) {
        _lastFrm = str;
    }

    public void initializeView() {
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        final ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setHomeAsUpIndicator(R.drawable.ic_menu_white_24dp);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        //registerReceiver(mHandleMessageReceiver, new IntentFilter("com.pasariklan.kutim.eletter.DISPLAY_MESSAGE"));
        if (! M.getSharedPref(this).getBoolean("GCMregistered", false)) {
            //registerInBackground();
        }
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mNavigationView = (NavigationView) findViewById(R.id.navigationView);
        layoutPercent = (LinearLayout) findViewById(R.id.layout_persentase);
        if(St.getInstance().getLogin().getData().getTotalNotifikasi() > 0){
            switchContent(new StafFragment());
            mNavigationView.getMenu().findItem(R.id.menuAktivitasBawahan).setChecked(true);
        }else{
            St.getInstance().setAktif_status("0");
            switchContent(new AktivitasFragment());
            mNavigationView.getMenu().findItem(R.id.menuAktivitasBaru).setChecked(true);
        }

        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                displayView(menuItem.getItemId());
                menuItem.setChecked(true);
                mDrawerLayout.closeDrawers();
                return true;
            }
        });
        updateDrawerHeaderInfo();
        ShortcutBadger.applyCount(MainActivity.this, St.getInstance().getLogin().getData().getTotalNotifikasi());
//        Toast.makeText(this, String.valueOf(St.getInstance().getLogin().getData().getLevelStruktural()),
//                Toast.LENGTH_LONG).show();
    }

    public void updateDrawerHeaderInfo() {
        updateHeaderView();
    }

    public void updateHeaderView() {
//        NavigationView navigationView = (NavigationView) findViewById(R.id.navigationView);
//        View headerView = navigationView.getHeaderView(0);

        ImageView profilePicture = (ImageView) findViewById(R.id.profilPic);
        Picasso.get()
                .load(R.drawable.logo_kemsos)
                .transform(new CropSquareTransformation())
                .placeholder(R.drawable.image_holder)
                .error(R.drawable.image_holder)
                .into(profilePicture);
        TextView userName = (TextView) findViewById(R.id.userName);
        TextView userJob = (TextView) findViewById(R.id.userJob);
        //ImageView profileCover = (ImageView) findViewById(R.id.profileCover);
        if(St.getInstance().getLogin()!=null) {
            if (St.getInstance().getLogin().getData() != null) {
                userJob.setText(St.getInstance().getLogin().getData().getJabatan());
                userName.setText(St.getInstance().getLogin().getData().getNama());
                Log.d("###", St.getInstance().getLogin().getData().getNama());

            }
        }

    }

    public void displayView(int id) {
        switch (id) {

            case R.id.menuAktivitasAdd:
//                if(St.getInstance().getLogin().getData().isStaf() == true || St.getInstance().getLogin().getData().isStrukturalSelainKasieKelurahan() == false){
//                    mFragment = new AddAktivitasFragment();
//                }
//                else if(St.getInstance().getLogin().getData().isKapdAsistenSekda() == true){
//                    mFragment = new AddAktivitasKapdAsistenSekdaFragment();
//                }
//                else{
//                    mFragment = new AddAktivitasStrukturalFragment();
//                }
                if(St.getInstance().getLogin().getData().isKapdAsistenSekda() == true){
                    mFragment = new AddAktivitasKapdAsistenSekdaFragment();
                }
                else{
                    mFragment = new AddAktivitasFragment();
                }
                break;

            case R.id.menuAktivitasBaru:
                layoutPercent.setVisibility(View.GONE);
                St.getInstance().setAktif_status("0");
                mFragment = new AktivitasFragment();
                break;
            case R.id.menuAktivitasTanpaFilePendukung:
                layoutPercent.setVisibility(View.GONE);
                St.getInstance().setAktif_status("102");
                mFragment = new AktivitasFragment();
                break;
            case R.id.menuAktivitasBawahan:
                layoutPercent.setVisibility(View.GONE);
                St.getInstance().setAktif_status("0");
                mFragment = new StafFragment();
                break;
            case R.id.menuAktivitasTelahDisahkan:
                layoutPercent.setVisibility(View.GONE);
                St.getInstance().setAktif_status("1");
                mFragment = new AktivitasFragment();
                break;
            case R.id.menuAktivitasRevisi:
                layoutPercent.setVisibility(View.GONE);
                St.getInstance().setAktif_status("100");
                mFragment = new AktivitasFragment();
                break;
            case R.id.menuAktivitasDitolak:
                layoutPercent.setVisibility(View.GONE);
                St.getInstance().setAktif_status("-1");
                mFragment = new AktivitasFragment();
                break;

            case R.id.logoutItem:
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                alertDialogBuilder.setMessage("Apakah Anda Yakin Ingin Keluar ?");
                alertDialogBuilder.setPositiveButton("Ya",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {
                                M.logOut(getApplicationContext());
                                //tess aj
                                //GCMRegistrar.checkDevice(this);
                                //GCMRegistrar.unregister(this);
                                //
                                ShortcutBadger.removeCount(getApplicationContext()); //for 1.1.4+
                                finish();
                                mIntent = new Intent(getApplicationContext(), LoginActivity.class);
                                startActivity(mIntent);
                            }
                        });

                alertDialogBuilder.setNegativeButton("Tidak",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {

                            }
                        });

                //Showing the alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();

                break;

            default:
                if(St.getInstance().getLogin().getData().getTotalNotifikasi() > 0){
                    mFragment = new StafFragment();
                } else {
                    mFragment = new AktivitasFragment();
                }
                break;
        }

        if (mFragment != null && mIntent == null) {
            //mFragmentManager = getFragmentManager();
            //St.getInstance().setFragmentManager(mFragmentManager);
            //mFragmentManager.beginTransaction().replace(R.id.frameContainer, mFragment).commit();
            switchContent(mFragment);
        } else {
            //startActivity(mIntent);
            //mIntent = null;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    public void switchContent(Fragment fragment) {
        mFragment = fragment;
        //getSupportFragmentManager().beginTransaction().replace(R.id.frameContainer, fragment).commit(); //v4
        MainActivity.this.getFragmentManager().beginTransaction().replace(R.id.frameContainer, fragment).commit();
        //slidemenu.showContent();
    }



//    private void registerInBackground() {
//        new AsyncTask<Void, Void, String>() {
//            @Override
//            protected String doInBackground(Void... params) {
//                String token;
//
//                try {
////                    InstanceID mInstanceID = InstanceID.getInstance(MainActivity.this);
////                    token = mInstanceID.getToken(AppConst.GOOGLE_PROJ_ID, GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
//                } catch (IOException ex) {
//                    M.L(ex.getMessage());
//                    token = null;
//                }
//                return token;
//            }
//
//            @Override
//            protected void onPostExecute(String token) {
//                updateRegID(token);
//            }
//        }.execute(null, null, null);
//    }

    public void updateRegID(String token) {
        if (token != null) {
            GlobalAPI mGlobalAPI = APIService.createService(GlobalAPI.class, M.getToken(this));
            mGlobalAPI.updateRegID(token, new Callback<ResponseModel>() {
                @Override
                public void success(ResponseModel response, Response response2) {
                    M.editSharedPref(MainActivity.this).putBoolean("GCMregistered", true).commit();
                }

                @Override
                public void failure(RetrofitError error) {
                    M.T(MainActivity.this, getString(R.string.ServerError));
                }
            });


            // badge
//            boolean success = ShortcutBadger.applyCount(this, +1); //for 1.1.4
//            Log.d(AppConst.TAG, "success="+success);
        }
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("Apakah Anda Ingin Keluar Aplikasi ?");
        alertDialogBuilder.setPositiveButton("Ya",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        finish();
                    }
                });

        alertDialogBuilder.setNegativeButton("Tidak",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {

                    }
                });

        //Showing the alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private final BroadcastReceiver mHandleMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            ArrayList newMessage = new ArrayList<String>();
            newMessage = intent.getStringArrayListExtra("message");
            //Toast.makeText(getApplicationContext(), "New Message: " + newMessage, Toast.LENGTH_LONG).show();
        }
    };


}
