package id.go.kemensos.eperformance.adapters;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdView;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import id.go.kemensos.eperformance.R;
import id.go.kemensos.eperformance.activities.EditAktivitas;
import id.go.kemensos.eperformance.activities.ZoomImage;
import id.go.kemensos.eperformance.api.APIService;
import id.go.kemensos.eperformance.api.GlobalAPI;
import id.go.kemensos.eperformance.data.AktivitasModel;
import id.go.kemensos.eperformance.data.St;
import id.go.kemensos.eperformance.helpers.M;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

public class AktivitasMenungguAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int TYPE_HEADER = 2;
    private static final int TYPE_ITEM = 1;
    private Activity mActivity;
    private List<AktivitasModel> mItems;
    private LayoutInflater mInflater;
    private SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy", Locale.US);

    public AktivitasMenungguAdapter(Activity mActivity, List<AktivitasModel> mItems) {
        this.mActivity = mActivity;
        mInflater = LayoutInflater.from(mActivity);
        this.mItems = mItems;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return TYPE_HEADER;
        } else {
            return TYPE_ITEM;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_HEADER) {
            View view = mInflater.inflate(R.layout.aktivitas_header, parent, false);
            HeaderViewHolder holder = new HeaderViewHolder(view);
            return holder;
        } else {
            View view = mInflater.inflate(R.layout.aktivitas_menunggu_list_item, parent, false);
            FindHolder holder = new FindHolder(view);
            return holder;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof FindHolder) {
            AktivitasModel item = mItems.get(position - 1);
            FindHolder findHolder = (FindHolder) holder;
            if (item.getAktivitasNama() != null) {
                findHolder.username.setText(item.getAktivitasNama());
                System.out.println("Aktivitas nama = " + item.getAktivitasNama());
            } else {
                findHolder.username.setText("-");
            }
            if (item.getKegiatanNama() != null) {
                findHolder.keg.setText(item.getKegiatanNama());
            } else {
                findHolder.keg.setText("-");
            }
            if (item.getCatatan() != null) {
                String sHtml = item.getCatatan();
                Spanned spanned = Html.fromHtml(sHtml);
                findHolder.status.setText("Catatan: \n" + spanned);
            } else {
                findHolder.status.setText("-");
            }
            if (item.getLokasiAlamat() != null) {
                findHolder.lokasi.setText(item.getLokasiAlamat());
            } else {
                findHolder.lokasi.setText("-");
            }

            if (item.getBeban().equalsIgnoreCase("") || item.getBeban().isEmpty() || item.getBeban() == null) {
                if (item.getJenisWaktuPengerjaan() == 0) {
                    findHolder.beban.setText(item.getWaktuPengerjaan() + " menit" + " (Pada Jam Kerja)");
                } else if (item.getJenisWaktuPengerjaan() == 1) {
                    findHolder.beban.setText(item.getWaktuPengerjaan() + " menit" + " (Diluar Jam Kerja)");
                }
            } else {
                if (item.getJenisWaktuPengerjaan() == 0) {
                    findHolder.beban.setText(item.getBeban() + " menit" + " (Pada Jam Kerja)");
                } else if (item.getJenisWaktuPengerjaan() == 1) {
                    findHolder.beban.setText(item.getBeban() + " menit" + " (Diluar Jam Kerja)");
                }
            }

            if (item.getFiles().size() > 0) {
                findHolder.newLayout.removeAllViews();
                for (int i = 0; i < item.getFiles().size(); i++) {
                    final String[] data = item.getFiles().get(i).split("\\|");
                    LinearLayout layout = new LinearLayout(mActivity);
                    layout.setOrientation(LinearLayout.HORIZONTAL);
                    if (data[2].toString().contains("image/")) {
                        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                                WRAP_CONTENT, 350);
                        layoutParams.setMargins(0, 10, 0, 10);
                        layoutParams.gravity = Gravity.CENTER;

                        TextView no = new TextView(mActivity);
                        ImageView gambar = new ImageView(mActivity);

                        no.setLayoutParams(new LinearLayout.LayoutParams(
                                WRAP_CONTENT,
                                WRAP_CONTENT));
                        no.setText("" + (i + 1) + ". ");
                        String test = "https://forums.androidcentral.com/images/forum_icons/200.png";
                        Picasso.get().load(data[3]).into(gambar);
                        gambar.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent FindIntent = new Intent(mActivity, ZoomImage.class);
                                FindIntent.putExtra("url", data[3]);
                                mActivity.startActivity(FindIntent);
                            }
                        });
                        if (item.getFiles().size() > 1) {
                            layout.addView(no);
                        }
                        layout.addView(gambar, layoutParams);
                    } else {
                        //String filename = getFileNameFromURL(data[3]);
                        TextView no = new TextView(mActivity);
                        TextView text = new TextView(mActivity);
                        no.setLayoutParams(new LinearLayout.LayoutParams(
                                WRAP_CONTENT,
                                WRAP_CONTENT));
                        no.setText("" + (i + 1) + ". ");
                        text.setLayoutParams(new LinearLayout.LayoutParams(
                                LinearLayout.LayoutParams.MATCH_PARENT,
                                LinearLayout.LayoutParams.MATCH_PARENT));
                        text.setText(data[1]);
                        if (item.getFiles().size() > 1) {
                            layout.addView(no);
                        }
                        layout.addView(text);
                    }
                    findHolder.newLayout.addView(layout);
                }
            } else {
                findHolder.newLayout.removeAllViews();
                TextView no = new TextView(mActivity);
                no.setLayoutParams(new LinearLayout.LayoutParams(
                        WRAP_CONTENT,
                        WRAP_CONTENT));
                no.setText(" -");
                findHolder.newLayout.addView(no);
            }

            String stgl = "";
            try {
                stgl = sdf.format(item.getTanggal());
            } catch (Exception e) {
            }
            findHolder.tanggal.setText(stgl);

            if (item.getKomentarAtasan() != null && item.getKomentarAtasan() != "" && item.getKomentarAtasan() != "null" && !item.getKomentarAtasan().isEmpty()) {
                findHolder.komentar.setText(item.getKomentarAtasan());
            } else {
                findHolder.komentar_lbl.setVisibility(View.GONE);
                findHolder.komentar.setVisibility(View.GONE);
            }
            /*Picasso.with(mActivity.getApplicationContext())
                    .load(item.getAvatar())
                    .transform(new CropSquareTransformation())
                    .error(R.drawable.image_holder)
                    .placeholder(R.drawable.image_holder)
                    .into(findHolder.picture);*/
            findHolder.nomor.setText(String.valueOf(item.getNomor()));
        } else if (holder instanceof HeaderViewHolder) {
            HeaderViewHolder headerHolder = (HeaderViewHolder) holder;
            {
                headerHolder.mAdView.setVisibility(View.GONE);
            }
        }

    }

    @Override
    public int getItemCount() {
        if (mItems != null) {
            return mItems.size() + 1;
        } else {
            return 0;
        }
    }

    public void setItems(List<AktivitasModel> itemModels) {
        this.mItems = itemModels;
        notifyDataSetChanged();
    }

    public List<AktivitasModel> getItems() {
        return this.mItems;
    }

    public class FindHolder extends RecyclerView.ViewHolder {
        TextView username, status, tanggal, nomor, keg, komentar_lbl, komentar, lokasi, beban;
        //ImageView picture;
        Button buttonEdit, buttonHapus;
        LinearLayout newLayout;

        public FindHolder(View v) {
            super(v);
            //picture = (ImageView) v.findViewById(R.id.picture);
            username = (TextView) v.findViewById(R.id.username);
            beban = (TextView) v.findViewById(R.id.beban);
            status = (TextView) v.findViewById(R.id.status);
            tanggal = (TextView) v.findViewById(R.id.tanggal);
            nomor = (TextView) v.findViewById(R.id.nomor);
            keg = (TextView) v.findViewById(R.id.keg);
            komentar_lbl = (TextView) v.findViewById(R.id.komentar_lbl);
            komentar = (TextView) v.findViewById(R.id.komentar);
            lokasi = (TextView) v.findViewById(R.id.lokasi);
            newLayout = (LinearLayout) v.findViewById(R.id.pendukung);
            buttonEdit = (Button) v.findViewById(R.id.btnEdit);
            buttonHapus = (Button) v.findViewById(R.id.btnHapus);
            buttonEdit.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    AktivitasModel item = mItems.get(FindHolder.this.getAdapterPosition() - 1);
                    if (St.getInstance().getLogin().getData().isKapdAsistenSekda() == true) {
                        if (St.getInstance().getAktif_status() == "1" || St.getInstance().getAktif_status() == "-1") {

                        } else {

                        }
                    } else {
//                        if (item.isStruktural()) {
//                            if (St.getInstance().getAktif_status() == "1" || St.getInstance().getAktif_status() == "-1") {
//
//                            } else {
//                                Intent FindIntent = new Intent(mActivity, EditAktivitasStruktural.class);
//                                FindIntent.putExtra("aktivitas_pegawai_id", item.getAktivitasPegawaiId());
//                                FindIntent.putExtra("aktivitas_id", item.getAktivitasId());
//                                FindIntent.putExtra("aktivitas_tanggal", item.getTanggal());
//                                FindIntent.putExtra("aktivitas_nama", item.getAktivitasNama());
//                                FindIntent.putExtra("aktivitas_waktu_pengerjaan", item.getWaktuPengerjaan());
//                                FindIntent.putExtra("aktivitas_jenis_waktu_pengerjaan", item.getJenisWaktuPengerjaan());
//                                FindIntent.putExtra("aktivitas_jumlah_output", item.getJumlahOutput());
//                                FindIntent.putExtra("aktivitas_satuan_output", item.getSatuanOutput());
//                                FindIntent.putExtra("aktivitas_kegiatan_id", item.getKegiatanId());
//                                FindIntent.putExtra("aktivitas_kegiatan_nama", item.getKegiatanNama());
//                                FindIntent.putExtra("aktivitas_catatan", item.getCatatan());
//                                FindIntent.putExtra("aktivitas_jenis", item.getJenis());
//                                FindIntent.putExtra("aktivitas_koordinat", item.getLokasiKoordinat());
//                                FindIntent.putExtra("aktivitas_alamat", item.getLokasiAlamat());
//                                FindIntent.putStringArrayListExtra("aktivitas_files", (ArrayList<String>) item.getFiles());
//                                mActivity.startActivity(FindIntent);
//                            }
//                        } else {
                            if (St.getInstance().getAktif_status() == "1" || St.getInstance().getAktif_status() == "-1") {

                            } else {
                                Intent EditIntent = new Intent(mActivity, EditAktivitas.class);
                                EditIntent.putExtra("aktivitas_pegawai_id", item.getAktivitasPegawaiId());
                                EditIntent.putExtra("aktivitas_id", item.getAktivitasId());
                                EditIntent.putExtra("aktivitas_tanggal", item.getTanggal());
                                EditIntent.putExtra("aktivitas_nama", item.getAktivitasNama());
                                EditIntent.putExtra("aktivitas_jenis_waktu_pengerjaan", item.getJenisWaktuPengerjaan());
                                EditIntent.putExtra("aktivitas_kegiatan_id", item.getKegiatanId());
                                EditIntent.putExtra("aktivitas_kegiatan_nama", item.getKegiatanNama());
                                EditIntent.putExtra("aktivitas_catatan", item.getCatatan());
                                EditIntent.putExtra("aktivitas_jenis", item.getJenis());
                                EditIntent.putExtra("aktivitas_koordinat", item.getLokasiKoordinat());
                                EditIntent.putExtra("aktivitas_alamat", item.getLokasiAlamat());
                                EditIntent.putStringArrayListExtra("aktivitas_files", (ArrayList<String>) item.getFiles());
                                mActivity.startActivity(EditIntent);
                            }
//                        }
                    }
                }
            });

            buttonHapus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mActivity);
                    alertDialogBuilder.setMessage("Apakah Anda Yakin Ingin Menghapus Aktivitas ini ?");
                    alertDialogBuilder.setPositiveButton("Ya",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface arg0, int arg1) {
                                    AktivitasModel item = mItems.get(FindHolder.this.getAdapterPosition() - 1);
                                    if (St.getInstance().getAktif_status() == "1" || St.getInstance().getAktif_status() == "-1") {

                                    } else {
                                        M.showLoadingDialog(mActivity);
                                        GlobalAPI api = APIService.createService(GlobalAPI.class, M.getToken(mActivity));
                                        api.hapus_aktivitas("doDeleteAktivitas",
                                                M.getToken(mActivity),
                                                item.getAktivitasPegawaiId(),
                                                new Callback<Response>() {
                                                    @Override
                                                    public void success(Response resultx, Response response) {
                                                        M.hideLoadingDialog();
                                                        //
                                                        BufferedReader reader = null;
                                                        StringBuilder sb = new StringBuilder();
                                                        try {
                                                            reader = new BufferedReader(new InputStreamReader(resultx.getBody().in()));
                                                            String line;
                                                            try {
                                                                while ((line = reader.readLine()) != null) {
                                                                    sb.append(line);
                                                                }
                                                            } catch (IOException e) {
                                                                e.printStackTrace();
                                                            }
                                                        } catch (IOException e) {
                                                            e.printStackTrace();
                                                        }
                                                        String res = sb.toString();
                                                        try {
                                                            JSONObject obj = new JSONObject(res);
                                                            JSONObject data = obj.getJSONObject("data");
                                                            Integer success = data.getInt("success");
                                                            String msg = data.getString("msg");
                                                            M.T(mActivity, msg);
                                                            if (success == 1) {
                                                                mItems.remove(FindHolder.this.getAdapterPosition() - 1);
                                                                notifyDataSetChanged();
                                                            }

                                                        } catch (Exception e) {
                                                            M.T(mActivity, e.getMessage());
                                                        }
                                                    }

                                                    @Override
                                                    public void failure(RetrofitError error) {
                                                        M.hideLoadingDialog();
                                                        M.T(mActivity, mActivity.getString(R.string.ServerError));
                                                        System.out.println("ERR: " + error.getMessage());
                                                    }
                                                });
                                    }
                                }
                            });

                    alertDialogBuilder.setNegativeButton("Tidak",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface arg0, int arg1) {

                                }
                            });

                    //Showing the alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                }
            });
        }

    }

    public class HeaderViewHolder extends RecyclerView.ViewHolder {
        public AdView mAdView;

        public HeaderViewHolder(View itemView) {
            super(itemView);
            mAdView = (AdView) itemView.findViewById(R.id.adView);
        }
    }

}
