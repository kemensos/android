package id.go.kemensos.eperformance.data;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AktivitasModel {
    int nomor=0, jenisWaktuPengerjaan = 0;
    String aktivitasPegawaiId,satuanOutput,waktuPengerjaan,jumlahOutput;
    Date tanggal;
    String pegawaiId, updatedAt, status, aktivitasId, aktivitasNama;
    String beban, catatan, komentarAtasan, kegiatanId, kegiatanNama, jenis, dariPegawai;
    String catatanAtasan;
    String lokasiKoordinat;
    String lokasiAlamat;
    List<String> files = new ArrayList<>();
    boolean isApproval8HariKebelakangTerkunci = true;

    public List<String> getFiles() {
        return files;
    }

    public void setFiles(List<String> files) {
        this.files = files;
    }

    public String getLokasiKoordinat() {
        return lokasiKoordinat;
    }

    public void setLokasiKoordinat(String lokasiKoordinat) {
        this.lokasiKoordinat = lokasiKoordinat;
    }

    public String getLokasiAlamat() {
        return lokasiAlamat;
    }

    public void setLokasiAlamat(String lokasiAlamat) {
        this.lokasiAlamat = lokasiAlamat;
    }

    boolean checked = false;
    boolean struktural = true;

    public boolean isStruktural() {
        return struktural;
    }

    public void setStruktural(boolean struktural) {
        this.struktural = struktural;
    }

    public String getWaktuPengerjaan() {
        return waktuPengerjaan;
    }

    public void setWaktuPengerjaan(String waktuPengerjaan) {
        this.waktuPengerjaan = waktuPengerjaan;
    }

    public int getJenisWaktuPengerjaan() {
        return jenisWaktuPengerjaan;
    }

    public void setJenisWaktuPengerjaan(int jenisWaktuPengerjaan) {
        this.jenisWaktuPengerjaan = jenisWaktuPengerjaan;
    }

    public String getJumlahOutput() {
        return jumlahOutput;
    }

    public void setJumlahOutput(String jumlahOutput) {
        this.jumlahOutput = jumlahOutput;
    }

    public String getSatuanOutput() {
        return satuanOutput;
    }

    public void setSatuanOutput(String satuanOutput) {
        this.satuanOutput = satuanOutput;
    }

    public boolean getChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public String getAktivitasPegawaiId() {
        return aktivitasPegawaiId;
    }

    public void setAktivitasPegawaiId(String aktivitasPegawaiId) {
        this.aktivitasPegawaiId = aktivitasPegawaiId;
    }

    public String getPegawaiId() {
        return pegawaiId;
    }

    public void setPegawaiId(String pegawaiId) {
        this.pegawaiId = pegawaiId;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Date getTanggal() {
        return tanggal;
    }

    public void setTanggal(Date tanggal) {
        this.tanggal = tanggal;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAktivitasId() {
        return aktivitasId;
    }

    public void setAktivitasId(String aktivitasId) {
        this.aktivitasId = aktivitasId;
    }

    public String getAktivitasNama() {
        return aktivitasNama;
    }

    public void setAktivitasNama(String aktivitasNama) {
        this.aktivitasNama = aktivitasNama;
    }

    public String getBeban() {
        return beban;
    }

    public void setBeban(String beban) {
        this.beban = beban;
    }

    public String getCatatan() {
        return catatan;
    }

    public void setCatatan(String catatan) {
        this.catatan = catatan;
    }

    public String getKomentarAtasan() {
        return komentarAtasan;
    }

    public void setKomentarAtasan(String komentarAtasan) {
        this.komentarAtasan = komentarAtasan;
    }

    public String getKegiatanId() {
        return kegiatanId;
    }

    public void setKegiatanId(String kegiatanId) {
        this.kegiatanId = kegiatanId;
    }

    public String getKegiatanNama() {
        return kegiatanNama;
    }

    public void setKegiatanNama(String kegiatanNama) {
        this.kegiatanNama = kegiatanNama;
    }

    public String getJenis() {
        return jenis;
    }

    public void setJenis(String jenis) {
        this.jenis = jenis;
    }

    public String getDariPegawai() {
        return dariPegawai;
    }

    public void setDariPegawai(String dariPegawai) {
        this.dariPegawai = dariPegawai;
    }

    public int getNomor() {
        return nomor;
    }

    public void setNomor(int nomor) {
        this.nomor = nomor;
    }

    public String getCatatanAtasan() {
        return catatanAtasan;
    }

    public void setCatatanAtasan(String catatanAtasan) {
        this.catatanAtasan = catatanAtasan;
    }

    public boolean isApproval8HariKebelakangTerkunci() {
        return isApproval8HariKebelakangTerkunci;
    }

    public void setApproval8HariKebelakangTerkunci(boolean approval8HariKebelakangTerkunci) {
        isApproval8HariKebelakangTerkunci = approval8HariKebelakangTerkunci;
    }
}
