package id.go.kemensos.eperformance.notificationService;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

import java.util.Random;

import id.go.kemensos.eperformance.R;
import id.go.kemensos.eperformance.activities.MainActivity;


public class MyNotificationManager {

    private Context mContext;
    private static MyNotificationManager myNotificationManager;

    private MyNotificationManager(Context context){
        mContext = context;
    }

    public static synchronized MyNotificationManager getInstance (Context context) {
        if(myNotificationManager == null){
            myNotificationManager = new MyNotificationManager(context);
        }
        return myNotificationManager;
    }

    public void displayNotification(String title, String body) {

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(mContext, Constants.CHANNEL_ID)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(title)
                        .setContentText(body);

        Intent intent = new Intent(mContext, MainActivity.class);

        PendingIntent pendingIntent = PendingIntent.getActivity(mContext, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        mBuilder.setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);

        if (notificationManager != null) {
            notificationManager.notify(new Random().nextInt(), mBuilder.build());
        }
    }

}
