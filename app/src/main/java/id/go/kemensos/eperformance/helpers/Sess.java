package id.go.kemensos.eperformance.helpers;



import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class Sess {
    private SharedPreferences sharedPref;
    private Editor editor;

    private static final String MY_AUTH_KEY = "o_auth_key";
    private static final String MY_USER_NAME = "o_user_name";
    private static final String SHARED = "My_Preferences";

    public Sess(Context context) {
        sharedPref 	  = context.getSharedPreferences(SHARED, Context.MODE_PRIVATE);
        editor 		  = sharedPref.edit();
    }

    public void store(String key, String username) {
        editor.putString(MY_AUTH_KEY, key);
        editor.putString(MY_USER_NAME, username);

        editor.commit();
    }

    public void reset() {
        editor.putString(MY_AUTH_KEY, null);
        editor.putString(MY_USER_NAME, null);

        editor.commit();
    }

    public String getAuthKey() {
        return sharedPref.getString(MY_AUTH_KEY, "");
    }

    public String getUsername() {
        return sharedPref.getString(MY_USER_NAME, "");
    }

    public void simpen(String key, String val) {
        editor.putString(key, val);
        editor.commit();
    }
    public String ambil(String key){
        String ret = "";
        try {
            ret = sharedPref.getString(key, "");
        }
        catch(Exception e) {
            //
        }

        return ret;
    }

}


