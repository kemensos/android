package id.go.kemensos.eperformance.adapters;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Html;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.ads.AdView;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import id.go.kemensos.eperformance.R;
import id.go.kemensos.eperformance.activities.ZoomImage;
import id.go.kemensos.eperformance.data.AktivitasOsModel;

import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

public class BawahanAktivitasOsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int TYPE_HEADER = 2;
    private static final int TYPE_ITEM = 1;
    private Activity mActivity;
    private List<AktivitasOsModel> mItems;
    private LayoutInflater mInflater;
    private SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy", Locale.US);


    public BawahanAktivitasOsAdapter(Activity mActivity, List<AktivitasOsModel> mItems) {
        this.mActivity = mActivity;
        mInflater = LayoutInflater.from(mActivity);
        this.mItems = mItems;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return TYPE_HEADER;
        } else {
            return TYPE_ITEM;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_HEADER) {
            View view = mInflater.inflate(R.layout.aktivitas_header, parent, false);
            HeaderViewHolder holder = new HeaderViewHolder(view);
            return holder;
        } else {
            View view = mInflater.inflate(R.layout.bawahan_aktivitas_list_item, parent, false);
            FindHolder holder = new FindHolder(view);
            return holder;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof FindHolder) {

            final AktivitasOsModel item = mItems.get(position - 1);
            final FindHolder findHolder = (FindHolder) holder;
            if (item.getAktivitasNama() != null) {
                findHolder.username.setText(item.getAktivitasNama());
            } else {
                findHolder.username.setText("-");
            }
            if (item.getKegiatanNama() != null) {
                findHolder.keg.setText(item.getKegiatanNama());
            } else {
                findHolder.keg.setText("-");
            }
            String stgl = "";
            String sExpired = "";
            if (item.isApproval8HariKebelakangTerkunci()) {
                try {
                    stgl = sdf.format(item.getTanggal());
                    long now = System.currentTimeMillis() - 1000;
                    //long tgl_min = now-(1000*60*60*24*7);
                    long tgl_min = now - (1000 * 60 * 60 * 24 * 8);
                    if (item.getTanggal().getTime() < tgl_min) { //data expired? 8 hari
                        findHolder.chk.setVisibility(View.INVISIBLE);
                        findHolder.catatanAtasan.setVisibility(View.INVISIBLE);
                        sExpired = " (<font color='#FF9900'>EXPIRED</font>)";
                    }
                } catch (Exception e) {
                }
            }
            String sHtml = "Catatan: <br>"+item.getCatatan()+sExpired;
            Spanned spanned = Html.fromHtml(sHtml);
            findHolder.status.setText(spanned);
            findHolder.tanggal.setText(stgl);
            if (item.getLokasiAlamat() != null) {
                findHolder.lokasi.setText(item.getLokasiAlamat());
            } else {
                findHolder.lokasi.setText("-");
            }
            if (item.getAktivitasBeban() != 0) {
                findHolder.beban.setText(item.getAktivitasBeban() + " menit (" + item.getJenis() + ")");
            } else {
                findHolder.beban.setText("-");
            }


            if(item.getFile().size() > 0) {
                findHolder.newLayout.removeAllViews();
                for (int i = 0; i < item.getFile().size(); i++) {
                    final String[] data = item.getFile().get(i).split("\\|");
                    LinearLayout layout = new LinearLayout(mActivity);
                    layout.setOrientation(LinearLayout.HORIZONTAL);
                    if (data[2].toString().contains("image/")) {
                        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                                WRAP_CONTENT, 350);
                        layoutParams.setMargins(0, 10, 0, 10);
                        layoutParams.gravity = Gravity.CENTER;

                        TextView no = new TextView(mActivity);
                        ImageView gambar = new ImageView(mActivity);

                        no.setLayoutParams(new LinearLayout.LayoutParams(
                                WRAP_CONTENT,
                                WRAP_CONTENT));
                        no.setText("" + (i + 1) + ". ");
                        String test = "https://forums.androidcentral.com/images/forum_icons/200.png";
                        Picasso.get().load(data[3]).into(gambar);
                        gambar.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent FindIntent = new Intent(mActivity, ZoomImage.class);
                                FindIntent.putExtra("url", data[3]);
                                mActivity.startActivity(FindIntent);
                            }
                        });
                        if(item.getFile().size() > 1){
                            layout.addView(no);
                        }
                        layout.addView(gambar,layoutParams);
                    } else {
                        //String filename = getFileNameFromURL(data[3]);
                        TextView no = new TextView(mActivity);
                        TextView text = new TextView(mActivity);
                        no.setLayoutParams(new LinearLayout.LayoutParams(
                                LinearLayout.LayoutParams.WRAP_CONTENT,
                                LinearLayout.LayoutParams.WRAP_CONTENT));
                        no.setText("" + (i + 1) + ". ");
                        text.setLayoutParams(new LinearLayout.LayoutParams(
                                LinearLayout.LayoutParams.MATCH_PARENT,
                                LinearLayout.LayoutParams.MATCH_PARENT));
                        text.setText(data[1]);
                        if(item.getFile().size() > 1){
                            layout.addView(no);
                        }
                        layout.addView(text);
                    }
                    findHolder.newLayout.addView(layout);
                }
            } else {
                findHolder.newLayout.removeAllViews();
                TextView no = new TextView(mActivity);
                no.setLayoutParams(new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT));
                no.setText(" -");
                findHolder.newLayout.addView(no);
            }

            findHolder.nomor.setText(String.valueOf(item.getNomor()));
            findHolder.catatanAtasan.setText(item.getCatatanAtasan());
            Log.i(this.getClass().getSimpleName(), "tanggal = "+item.getTanggal());
            //
            findHolder.chk.setChecked(false);
            if (item.isChecked()) {
                findHolder.chk.setChecked(true);
            }
            findHolder.chk.setOnCheckedChangeListener(null);
            findHolder.chk.setTag(position);
            /*findHolder.chk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Integer pos = (Integer) findHolder.chk.getTag();
                    AktivitasModel item = mItems.get(pos - 1);
                    if (item.getChecked()) item.setChecked(false);
                    else item.setChecked(true);
                    Log.d(AppConst.TAG, findHolder.chk.getTag()+" chk posisi="+pos+":"+item.getNomor()+":"+item.getAktivitasNama());
                }
            });*/
            findHolder.chk.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    item.setChecked(b);
                    for (AktivitasOsModel value : mItems) {
                        if (value.isChecked()) {
                            if (findHolder.targetStatus.getVisibility() == View.GONE) {
                                findHolder.targetStatus.setVisibility(View.VISIBLE);
                            }
                            if (findHolder.btnSave.getVisibility() == View.GONE) {
                                findHolder.btnSave.setVisibility(View.VISIBLE);
                            }
                            break;
                        }
                        if (findHolder.targetStatus.getVisibility() == View.VISIBLE) {
                            findHolder.targetStatus.setVisibility(View.GONE);
                        }
                        if (findHolder.btnSave.getVisibility() == View.VISIBLE) {
                            findHolder.btnSave.setVisibility(View.GONE);
                        }
                    }
                }
            });

            findHolder.catatanAtasan.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    item.setCatatanAtasan(editable.toString());
                }
            });
        } else if (holder instanceof HeaderViewHolder) {
            HeaderViewHolder headerHolder = (HeaderViewHolder) holder;
            {
                headerHolder.mAdView.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public int getItemCount() {
        if (mItems != null) {
            return mItems.size() + 1;
        } else {
            return 0;
        }
    }

    public void setItems(List<AktivitasOsModel> itemModels) {
        this.mItems = itemModels;
        notifyDataSetChanged();
    }

    //public class FindHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    public class FindHolder extends RecyclerView.ViewHolder {
        TextView username, status, tanggal, nomor, keg, lokasi, beban;
        CheckBox chk;
        EditText catatanAtasan;
        LinearLayout newLayout;
        Spinner targetStatus;
        Button btnSave;

        public FindHolder(View v) {
            super(v);
            username = (TextView) v.findViewById(R.id.username);
            beban = (TextView) v.findViewById(R.id.beban);
            status = (TextView) v.findViewById(R.id.status);
            tanggal = (TextView) v.findViewById(R.id.tanggal);
            nomor = (TextView) v.findViewById(R.id.nomor);
            keg = (TextView) v.findViewById(R.id.keg);
            lokasi = (TextView) v.findViewById(R.id.lokasi);
            newLayout = (LinearLayout) v.findViewById(R.id.pendukung);
            catatanAtasan = (EditText) v.findViewById(R.id.catatanAtasan);
            chk = (CheckBox) v.findViewById(R.id.chk);
            targetStatus = (Spinner) mActivity.findViewById(R.id.spTargetStatus);
            btnSave = (Button) mActivity.findViewById(R.id.btnSave);
            this.setIsRecyclable(false); //
            //chk.setOnClickListener(this);
            //v.setOnClickListener(this);
        }

        //@Override
        //public void onClick(View v) {
            /*AktivitasModel item = mItems.get(this.getPosition() - 1);
            Intent FindIntent = new Intent(mActivity, ProfilePreview.class);
            FindIntent.putExtra("aktivitasId", item.getAktivitasId());
            mActivity.startActivity(FindIntent);*/
            /*if(v.getId()==R.id.chk) {
                AktivitasModel item = mItems.get(this.getLayoutPosition() - 1);
                if (chk.isChecked()) item.setChecked(true);
                else item.setChecked(false);
                Log.d(AppConst.TAG, "chk="+this.getLayoutPosition()+":"+item.getNomor()+":"+item.getAktivitasNama());
            }*/
        //}
    }

    public class HeaderViewHolder extends RecyclerView.ViewHolder {
        public AdView mAdView;

        public HeaderViewHolder(View itemView) {
            super(itemView);
            mAdView = (AdView) itemView.findViewById(R.id.adView);
        }
    }
}
