package id.go.kemensos.eperformance.data;

/**
 * Created by enith on 10/4/2015.
 */
public class SubjectModel {
    int id;
    String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
