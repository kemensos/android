package id.go.kemensos.eperformance.notificationService;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.graphics.Color;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import id.go.kemensos.eperformance.R;
import id.go.kemensos.eperformance.data.St;
import me.leolin.shortcutbadger.ShortcutBadger;


public class MyFirebaseMessagingService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        //super.onMessageReceived(remoteMessage);

        String title = getResources().getString(R.string.app_name);
        String body = remoteMessage.getNotification().getBody();
//        String[] staff = remoteMessage.getNotification().getBody().split("#");

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationManager mNotificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            NotificationChannel mChannel = new NotificationChannel(Constants.CHANNEL_ID, Constants.CHANNEL_NAME, NotificationManager.IMPORTANCE_HIGH);
            mChannel.setDescription(Constants.CHANNEL_DESCRIPTION);
            mChannel.enableLights(true);
            mChannel.setLightColor(Color.RED);
            mChannel.enableVibration(true);
            mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            mNotificationManager.createNotificationChannel(mChannel);
        }

        MyNotificationManager.getInstance(getApplicationContext()).displayNotification(title, body);
//        MyNotificationManager.getInstance(getApplicationContext()).displayNotification(title, Arrays.asList(staff));

        St.getInstance().getLogin().getData().setTotalNotifikasi(St.getInstance().getLogin().getData().getTotalNotifikasi() + 1);
        ShortcutBadger.applyCount(getApplicationContext(), St.getInstance().getLogin().getData().getTotalNotifikasi());
    }
}
