package id.go.kemensos.eperformance.activities;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import id.go.kemensos.eperformance.R;
import id.go.kemensos.eperformance.adapters.BawahanAktivitasAdapter;
import id.go.kemensos.eperformance.api.APIService;
import id.go.kemensos.eperformance.api.GlobalAPI;
import id.go.kemensos.eperformance.app.AppConst;
import id.go.kemensos.eperformance.data.AktivitasModel;
import id.go.kemensos.eperformance.data.KonfirmasiAktivitasModel;
import id.go.kemensos.eperformance.data.St;
import id.go.kemensos.eperformance.helpers.M;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import me.leolin.shortcutbadger.ShortcutBadger;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class BahawanAktivitas extends Activity implements View.OnClickListener {
    public RecyclerView mList;
    public LinearLayoutManager layoutManager;
    public List<AktivitasModel> mItems = new ArrayList<AktivitasModel>();
    public BawahanAktivitasAdapter mAdapter;
    public Intent mIntent = null;
    public Button searchBtn, btnSave;
    TextView nama;

    SimpleDateFormat dateFormatter;
    private boolean kuncian;

    Spinner searchBulan, spStatus, spTargetStatus;
    String[] items = {"Januari","Febuari","Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "Nopember", "Desember"};
    String[] statuss = {"Menunggu Pengesahan", "Telah Disahkan", "Perlu Revisi", "Ditolak"};
    String[] targetStatuss = {"Sahkan", "Perlu Revisi", "Ditolak"};

    int page=1, jml_per_page=25;
    String target_status="";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bawahan_aktivitas2);

        this.setTitle("Verifikasi Aktivitas Bawahan");
        St.getInstance().getMain().setLastFrm("staf");

        nama = (TextView)findViewById(R.id.nama);
        nama.setText(St.getInstance().getAktif_bawahan_nama());

        dateFormatter = new SimpleDateFormat("MM", Locale.US);
        Date date = new Date();
        searchBulan = (Spinner)this.findViewById(R.id.searchBulan);
        searchBulan.setAdapter(new BlnAdapter(BahawanAktivitas.this, R.layout.row_spinner, items));

        btnSave = (Button) this.findViewById(R.id.btnSave);
        btnSave.setOnClickListener(this);
        //
        Integer pos = Integer.parseInt(dateFormatter.format(date));
        searchBulan.setSelection(pos - 1);
        St.getInstance().setAktif_bulan(pos.toString());

        searchBulan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Integer pos = (searchBulan.getSelectedItemPosition() + 1);
                St.getInstance().setAktif_bulan(pos.toString());
                cekKuncian(pos);
                //search();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spStatus = (Spinner)this.findViewById(R.id.spStatus);
        spStatus.setAdapter(new StatusAdapter(BahawanAktivitas.this, R.layout.row_spinner, statuss));
        spStatus.setSelection(0);
        St.getInstance().setAktif_status("0");
        spStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Integer pos = spStatus.getSelectedItemPosition();
                String status = "0";
                targetStatuss = new String[3];
                targetStatuss[0] = "Sahkan";
                targetStatuss[1] = "Perlu Revisi";
                targetStatuss[2] = "Ditolak";
                if (pos == 0) {
                    status = "0";
                } else if (pos == 1) {
                    status = "1";
                    targetStatuss = new String[1];
                    targetStatuss[0] = "Dibatalkan";
                } else if (pos == 2) {
                    status = "100";
                } else if (pos == 3) {
                    status = "-1";
                }
                Log.d(AppConst.TAG, "status:"+status);
                St.getInstance().setAktif_status(status);
                //search();
                bind_target();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        bind_target();


        mList = (RecyclerView) this.findViewById(R.id.friendsList);
        searchBtn = (Button) this.findViewById(R.id.searchBtn);
        searchBtn.setOnClickListener(this);

        //layout manager
        layoutManager = new LinearLayoutManager(BahawanAktivitas.this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mAdapter = new BawahanAktivitasAdapter(BahawanAktivitas.this, mItems);
        mList.setLayoutManager(layoutManager);
        mList.setAdapter(mAdapter);//set the adapter
        //


        search();
    }

    void bind_target()
    {
        target_status = "1";
        spTargetStatus = (Spinner)this.findViewById(R.id.spTargetStatus);
        spTargetStatus.setAdapter(new TargetStatusAdapter(BahawanAktivitas.this, R.layout.row_spinner, targetStatuss));
        spTargetStatus.setSelection(0);
        spTargetStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String str = spTargetStatus.getSelectedItem().toString();
                Integer pos = spTargetStatus.getSelectedItemPosition();
                String status = "1";
                if (str.equalsIgnoreCase("Sahkan")) {
                    status = "1";
                } else if (str.equalsIgnoreCase("Perlu Revisi")) {
                    status = "100";
                } else if (str.equalsIgnoreCase("Ditolak")) {
                    status = "-1";
                }
                else if (str.equalsIgnoreCase("Dibatalkan")) {
                    status = "0";
                }
                Log.d(AppConst.TAG, "status:"+status);
                target_status = status;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void getBadge() {
        GlobalAPI api = APIService.createService(GlobalAPI.class, M.getToken(this));
        api.get_badge("getTotalNotifikasi",
                M.getToken(this), St.getInstance().getLogin().getData().getPegawaiId(),
                new Callback<Response>() {
                    @Override
                    public void success(Response resultx, Response response) {
                        M.hideLoadingDialog();
                        //
                        BufferedReader reader = null;
                        StringBuilder sb = new StringBuilder();
                        try {
                            reader = new BufferedReader(new InputStreamReader(resultx.getBody().in()));
                            String line;
                            try {
                                while ((line = reader.readLine()) != null) {
                                    sb.append(line);
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        String res = sb.toString();
                        try {
                            JSONObject obj = new JSONObject(res);
                            JSONObject data = obj.getJSONObject("data");
                            Integer success = data.getInt("success");
                            if (success > 0) {
                                St.getInstance().getLogin().getData().setTotalNotifikasi(data.getInt("totalNotifikasi"));
                                ShortcutBadger.applyCount(getApplicationContext(), St.getInstance().getLogin().getData().getTotalNotifikasi());
//                                    boolean badgeSuccess = ShortcutBadger.applyCount(MainActivity.this, total.getTotal()); //for 1.1.4+
//                                    Log.d(AppConst.TAG, "success="+badgeSuccess);
                            }
                        } catch (Exception e) {
                            M.T(getApplicationContext(), e.getMessage());
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        M.hideLoadingDialog();
                        M.T(getApplicationContext(), getString(R.string.ServerError));
                        System.out.println("ERR: " + error.getMessage());
                    }
                });

    }

    private void cekKuncian(Integer bulan) {
        GlobalAPI api = APIService.createService(GlobalAPI.class, M.getToken(this));
        api.cek_kuncian("cekKuncian", M.getToken(this), bulan, St.getInstance().getLogin().getData().getPegawaiId(),
                new Callback<Response>() {
                    @Override
                    public void success(Response resultx, Response response) {
                        //
                        BufferedReader reader = null;
                        StringBuilder sb = new StringBuilder();
                        try {
                            reader = new BufferedReader(new InputStreamReader(resultx.getBody().in()));
                            String line;
                            try {
                                while ((line = reader.readLine()) != null) {
                                    sb.append(line);
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        String res = sb.toString();
                        try {
                            JSONObject obj = new JSONObject(res);
                            JSONObject data = obj.getJSONObject("data");
                            Boolean success = data.getBoolean("success");
                            kuncian = success;
                            if (kuncian == true) {
                                btnSave.setEnabled(true);
                            } else {
                                btnSave.setEnabled(false);
                                M.T(BahawanAktivitas.this, data.getString("msg"));
                            }
                        } catch (Exception e) {
                            M.T(BahawanAktivitas.this, e.getMessage());
                            kuncian = false;
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        kuncian = false;
                        M.T(BahawanAktivitas.this, getString(R.string.ServerError));
                        System.out.println("ERR: " + error.getMessage());
                    }
                });
    }

    public void search() {
        spTargetStatus.setVisibility(View.GONE);
        btnSave.setVisibility(View.GONE);
        //https://eperformance.surabaya.go.id/wsdroid-testbed/android.shtml?obj[action]=getAktivitasPegawaiFilterByBulanOrStatus
        M.showLoadingDialog(BahawanAktivitas.this);
        GlobalAPI api = APIService.createService(GlobalAPI.class, M.getToken(BahawanAktivitas.this));
        api.bawahan_aktivitas_lst("getAktivitasPegawaiFilterByBulanOrStatus",
                M.getToken(BahawanAktivitas.this),
                St.getInstance().getAktif_bawahan_id(),
                St.getInstance().getAktif_bulan(),
                St.getInstance().getAktif_status(),
                page, 1000, //jml_per_page,
                new Callback<Response>() {
                    @Override
                    public void success(Response resultx, retrofit.client.Response response) {
                        M.hideLoadingDialog();
                        //
                        BufferedReader reader = null;
                        StringBuilder sb = new StringBuilder();
                        try {
                            reader = new BufferedReader(new InputStreamReader(resultx.getBody().in()));
                            String line;
                            try {
                                while ((line = reader.readLine()) != null) {
                                    sb.append(line);
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        String res = sb.toString();
                        try{
                            JSONObject obj = new JSONObject(res);
                            JSONObject data = obj.getJSONObject("data");
                            Integer success = data.getInt("success");
                            if(success==-1) {
                                M.logOut(BahawanAktivitas.this);
                                mIntent = new Intent(BahawanAktivitas.this, LoginActivity.class);
                                startActivity(mIntent);
                                //finish();
                            }
                            mItems = new ArrayList<AktivitasModel>();
                            if (success>0) {
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
                                Date dt = new Date();
                                JSONArray hasils = data.getJSONArray("aktivitas");
                                for (int i = 0; i < hasils.length(); i++) {
                                    AktivitasModel tmp = new AktivitasModel();
                                    tmp.setAktivitasPegawaiId(hasils.getJSONObject(i).getString("aktivitasPegawaiId"));
                                    tmp.setPegawaiId(hasils.getJSONObject(i).getString("pegawaiId"));
                                    tmp.setUpdatedAt(hasils.getJSONObject(i).getString("updatedAt"));
                                    try {
                                        dt = sdf.parse(hasils.getJSONObject(i).getString("tanggal"));
                                        tmp.setTanggal(dt);
                                    }
                                    catch(Exception e) {}
                                    tmp.setStatus(hasils.getJSONObject(i).getString("status"));
                                    tmp.setAktivitasId(hasils.getJSONObject(i).getString("aktivitasId"));
                                    if(St.getInstance().getLogin().getData().isStaf() == true || St.getInstance().getLogin().getData().isStrukturalSelainKasieKelurahan() == false){
                                        tmp.setAktivitasNama(hasils.getJSONObject(i).getString("aktivitasNama"));
                                    }
                                    else {
                                        if(hasils.getJSONObject(i).getString("aktivitasId").equalsIgnoreCase("null") ){
                                            tmp.setAktivitasNama(hasils.getJSONObject(i).getString("aktivitas_string"));
                                        }
                                        else{
                                            tmp.setAktivitasNama(hasils.getJSONObject(i).getString("aktivitasNama"));
                                        }
                                    }
                                    tmp.setBeban(hasils.getJSONObject(i).getString("beban"));
                                    tmp.setCatatan(hasils.getJSONObject(i).getString("catatan"));
                                    tmp.setKomentarAtasan(hasils.getJSONObject(i).getString("komentarAtasan"));
                                    tmp.setKegiatanId(hasils.getJSONObject(i).getString("kegiatanId"));
                                    tmp.setKegiatanNama(hasils.getJSONObject(i).getString("kegiatanNama"));
                                    tmp.setJenis(hasils.getJSONObject(i).getString("jenis"));
                                    tmp.setDariPegawai(hasils.getJSONObject(i).getString("dariPegawai"));
                                    tmp.setJenisWaktuPengerjaan(hasils.getJSONObject(i).getInt("jenis_waktu_pengerjaan"));
                                    tmp.setNomor(i+1);
                                    tmp.setLokasiAlamat(hasils.getJSONObject(i).getString("lokasi_alamat"));
                                    tmp.setLokasiKoordinat(hasils.getJSONObject(i).getString("lokasi_koordinat"));
                                    tmp.setApproval8HariKebelakangTerkunci(data.getBoolean("isApproval8HariKebelakangTerkunci"));
                                    ArrayList<String> stringArray = new ArrayList<String>();
                                    JSONArray jsonArray = hasils.getJSONObject(i).getJSONArray("files");
                                    for(int j = 0; j < jsonArray.length(); j++)
                                    {
                                        try {
                                            stringArray.add(String.valueOf(jsonArray.get(j)));
                                        }
                                        catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                    tmp.setFiles(stringArray);

                                    mItems.add(tmp);
                                }
                            }
                            mAdapter.setItems(mItems);
                        }
                        catch (Exception e){
                            M.T(BahawanAktivitas.this, e.getMessage());
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        M.hideLoadingDialog();
                        M.T(BahawanAktivitas.this, getString(R.string.ServerError));
                        System.out.println("ERR: " + error.getMessage());
                    }
                });

    }

    public class BlnAdapter extends ArrayAdapter<String>
    {

        public BlnAdapter(Context context, int textViewResourceId, String[] objects)
        {
            super(context, textViewResourceId, objects);
        }

        @Override
        public View getDropDownView(int position, View convertView,ViewGroup parent)
        {
            return getCustomView(position, convertView, parent);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            return getCustomView(position, convertView, parent);
        }

        public View getCustomView(int position, View convertView, ViewGroup parent)
        {

            LayoutInflater inflater=BahawanAktivitas.this.getLayoutInflater();
            View row=inflater.inflate(R.layout.row_spinner, parent, false);
            TextView label=(TextView)row.findViewById(R.id.company);
            label.setText(items[position]);

            //TextView sub=(TextView)row.findViewById(R.id.sub);
            //sub.setText(subs[position]);

            //ImageView icon=(ImageView)row.findViewById(R.id.image);
            //icon.setImageResource(arr_images[position]);

            return row;
        }
    }

    public class StatusAdapter extends ArrayAdapter<String>
    {
        public StatusAdapter(Context context, int textViewResourceId, String[] objects)
        {
            super(context, textViewResourceId, objects);
        }
        @Override
        public View getDropDownView(int position, View convertView,ViewGroup parent)
        {
            return getCustomView(position, convertView, parent);
        }
        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            return getCustomView(position, convertView, parent);
        }
        public View getCustomView(int position, View convertView, ViewGroup parent)
        {
            LayoutInflater inflater=BahawanAktivitas.this.getLayoutInflater();
            View row=inflater.inflate(R.layout.row_spinner, parent, false);
            TextView label=(TextView)row.findViewById(R.id.company);
            label.setText(statuss[position]);
            return row;
        }
    }

    public class TargetStatusAdapter extends ArrayAdapter<String>
    {
        public TargetStatusAdapter(Context context, int textViewResourceId, String[] objects)
        {
            super(context, textViewResourceId, objects);
        }
        @Override
        public View getDropDownView(int position, View convertView,ViewGroup parent)
        {
            return getCustomView(position, convertView, parent);
        }
        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            return getCustomView(position, convertView, parent);
        }
        public View getCustomView(int position, View convertView, ViewGroup parent)
        {
            LayoutInflater inflater=BahawanAktivitas.this.getLayoutInflater();
            View row=inflater.inflate(R.layout.row_spinner, parent, false);
            TextView label=(TextView)row.findViewById(R.id.company);
            label.setText(targetStatuss[position]);
            return row;
        }
    }



    public void konfirm() {
        List<KonfirmasiAktivitasModel> datas = new ArrayList<KonfirmasiAktivitasModel>();
        int i = 0;
        Map<String, String> maps = new HashMap<String, String>();

        JSONObject data = new JSONObject();
        JSONArray arr = new JSONArray();

        for(AktivitasModel aktivitasModel: mItems){
            if (aktivitasModel.getChecked()) {
                try {
                    data = new JSONObject();
                    data.put("aktivitasPegawaiId", aktivitasModel.getAktivitasPegawaiId());
                    data.put("catatanAtasan", aktivitasModel.getCatatanAtasan());
                    arr.put(data);
                }
                catch (Exception ex){}

            }
        }


        M.showLoadingDialog(BahawanAktivitas.this);
        GlobalAPI api = APIService.createService(GlobalAPI.class, M.getToken(BahawanAktivitas.this));
        api.do_verify("doVerify",
                M.getToken(BahawanAktivitas.this),
                arr.toString(), target_status,
                new Callback<Response>() {
                    @Override
                    public void success(Response resultx, retrofit.client.Response response) {
                        M.hideLoadingDialog();
                        //
                        BufferedReader reader = null;
                        StringBuilder sb = new StringBuilder();
                        try {
                            reader = new BufferedReader(new InputStreamReader(resultx.getBody().in()));
                            String line;
                            try {
                                while ((line = reader.readLine()) != null) {
                                    sb.append(line);
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        String res = sb.toString();
                        try {
                            JSONObject obj = new JSONObject(res);
                            JSONObject data = obj.getJSONObject("data");
                            Integer success = data.getInt("success");
                            if(success==0){
                                M.T(BahawanAktivitas.this, data.getString("msg"));
                                getBadge();
                            }
                            else if(success==1){
                                M.T(BahawanAktivitas.this, data.getString("msg"));
                                getBadge();
                                finish();
                            }
                        } catch (Exception e) {
                            M.T(BahawanAktivitas.this, e.getMessage());
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        M.hideLoadingDialog();
                        M.T(BahawanAktivitas.this, getString(R.string.ServerError));
                        System.out.println("ERR: " + error.getMessage());
                    }
                });

    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.searchBtn) {
            //Integer pos = (searchBulan.getSelectedItemPosition()+1);
            //St.getInstance().setAktif_bulan(pos.toString());
            search();
        }
        else if(v.getId() == R.id.btnSave){
            konfirm();
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
