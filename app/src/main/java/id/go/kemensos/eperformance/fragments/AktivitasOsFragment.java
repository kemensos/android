package id.go.kemensos.eperformance.fragments;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import id.go.kemensos.eperformance.GPSTracker;
import id.go.kemensos.eperformance.R;
import id.go.kemensos.eperformance.adapters.AktivitasOsAdapter;
import id.go.kemensos.eperformance.adapters.AktivitasMenungguOsAdapter;
import id.go.kemensos.eperformance.api.APIService;
import id.go.kemensos.eperformance.api.GlobalAPI;
import id.go.kemensos.eperformance.data.AktivitasOsModel;
import id.go.kemensos.eperformance.data.St;
import id.go.kemensos.eperformance.helpers.M;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class AktivitasOsFragment extends Fragment implements
        OnClickListener {
    public RecyclerView rowsList;
    public LinearLayoutManager layoutManager;
    public List<AktivitasOsModel> mItems = new ArrayList<AktivitasOsModel>();
    public AktivitasOsAdapter mAdapter;
    public AktivitasMenungguOsAdapter aMadapter;
    public Intent mIntent = null;
    public EditText searchField;
    public ImageButton searchBtn;
    private View mView;
    SimpleDateFormat dateFormatter;
    Spinner searchBulan;
    String[] items = {"Januari", "Febuari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "Nopember", "Desember"};
    //private SwipeRefreshLayout mSwipeRefreshLayout;
    int currentPage = 1;
    String currentSearch = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_aktivitas, container,
                false);
        this.getActivity().setTitle("Aktivitas");

        initializeView();

        GPSTracker gps = new GPSTracker(getActivity());
        if (!gps.canGetLocation()) {
            gps.showSettingsAlert();
        }

        //dateFormatter = new SimpleDateFormat("MM", Locale.US);
        dateFormatter = new SimpleDateFormat("M", Locale.US);
        Date date = new Date();
        searchBulan = (Spinner) mView.findViewById(R.id.searchBulan);
        searchBulan.setAdapter(new MyAdapter(getActivity(), R.layout.row_spinner, items));
        //searchBulan.setOnClickListener(this);
        Integer pos = Integer.parseInt(dateFormatter.format(date));
        searchBulan.setSelection(pos - 1);
        St.getInstance().setAktif_bulan(pos.toString());


        if (St.getInstance().getAktif_status().equalsIgnoreCase("0")) {
            this.getActivity().setTitle("Menunggu Pengesahan");
        } else if (St.getInstance().getAktif_status().equalsIgnoreCase("102")) {
            this.getActivity().setTitle("Tanpa File Pendukung");
        } else if (St.getInstance().getAktif_status().equalsIgnoreCase("1")) {
            this.getActivity().setTitle("Telah Disahkan");
        } else if (St.getInstance().getAktif_status().equalsIgnoreCase("100")) {
            this.getActivity().setTitle("Perlu Revisi");
        } else if (St.getInstance().getAktif_status().equalsIgnoreCase("-1")) {
            this.getActivity().setTitle("Ditolak");
        }

        searchBulan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String searchValue = searchField.getText().toString().trim();

                Integer pos = (searchBulan.getSelectedItemPosition() + 1);
                St.getInstance().setAktif_bulan(pos.toString());
                search(searchValue, 1);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        return mView;
    }


    public class MyAdapter extends ArrayAdapter<String> {

        public MyAdapter(Context context, int textViewResourceId, String[] objects) {
            super(context, textViewResourceId, objects);
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        public View getCustomView(int position, View convertView, ViewGroup parent) {

            LayoutInflater inflater = getActivity().getLayoutInflater();
            View row = inflater.inflate(R.layout.row_spinner, parent, false);
            TextView label = (TextView) row.findViewById(R.id.company);
            label.setText(items[position]);

            //TextView sub=(TextView)row.findViewById(R.id.sub);
            //sub.setText(subs[position]);

            //ImageView icon=(ImageView)row.findViewById(R.id.image);
            //icon.setImageResource(arr_images[position]);

            return row;
        }
    }


    public void initializeView() {
        rowsList = (RecyclerView) mView.findViewById(R.id.friendsList);
        searchField = (EditText) mView.findViewById(R.id.searchField);
        searchBtn = (ImageButton) mView.findViewById(R.id.searchBtn);
        //layout manager
        layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        if (St.getInstance().getAktif_status().equalsIgnoreCase("0") ||
                St.getInstance().getAktif_status().equalsIgnoreCase("102") || St.getInstance().getAktif_status().equalsIgnoreCase("100")) {
            aMadapter = new AktivitasMenungguOsAdapter(getActivity(), mItems);
        } else {
            mAdapter = new AktivitasOsAdapter(getActivity(), mItems);
        }
        rowsList.setLayoutManager(layoutManager);
        if (St.getInstance().getAktif_status().equalsIgnoreCase("0") ||
                St.getInstance().getAktif_status().equalsIgnoreCase("102") || St.getInstance().getAktif_status().equalsIgnoreCase("100")) {
            rowsList.setAdapter(aMadapter);//set the adapter
        } else {
            rowsList.setAdapter(mAdapter);//set the adapter
        }
        searchBtn.setOnClickListener(this);
    }

    public void search(String value, Integer pg) {
        String status = St.getInstance().getAktif_status();
        M.showLoadingDialog(getActivity());
        GlobalAPI api = APIService.createService(GlobalAPI.class, M.getToken(getActivity()));
        api.aktivitas_os("getAktivitasPegawaiByBulanAndStatus", M.getToken(getActivity()),
                St.getInstance().getLoginOs().getData().getPegawai_id(), St.getInstance().getAktif_bulan(), status,
                new Callback<Response>() {
            @Override
            public void success(Response resultx, Response response) {
                //
                BufferedReader reader = null;
                StringBuilder sb = new StringBuilder();
                try {
                    reader = new BufferedReader(new InputStreamReader(resultx.getBody().in()));
                    String line;
                    try {
                        while ((line = reader.readLine()) != null) {
                            sb.append(line);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                String res = sb.toString();
                try {
                    JSONObject obj = new JSONObject(res);
                    JSONObject data = obj.getJSONObject("data");
                    boolean success = obj.getBoolean("success");
                    mItems = new ArrayList<AktivitasOsModel>();
                    if (success) {
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
                        Date dt = new Date();
                        Integer g_no = (getCurrentPage() - 1) * Integer.parseInt(St.getInstance().getJml_per_page());
                        JSONArray hasils = data.getJSONArray("arrAktivitas");
                        for (int i = 0; i < hasils.length(); i++) {
                            AktivitasOsModel tmp = new AktivitasOsModel();
                            tmp.setId(hasils.getJSONObject(i).getInt("id"));
                            tmp.setUpdatedAt(hasils.getJSONObject(i).getString("updatedAt"));
                            try {
                                dt = sdf.parse(hasils.getJSONObject(i).getString("tanggal"));
                                tmp.setTanggal(dt);
                            } catch (Exception e) {

                            }
                            tmp.setStatus(hasils.getJSONObject(i).getInt("status"));
                            tmp.setStatusNama(hasils.getJSONObject(i).getString("statusNama"));
                            tmp.setAktivitasId(hasils.getJSONObject(i).getInt("aktivitasId"));
                            tmp.setAktivitasNama(hasils.getJSONObject(i).getString("aktivitasNama"));
                            tmp.setAktivitasBeban(hasils.getJSONObject(i).getInt("aktivitasBeban"));
                            tmp.setAktivitasSatuan(hasils.getJSONObject(i).getString("aktivitasSatuan"));
                            tmp.setKegiatanId(hasils.getJSONObject(i).getInt("kegiatanId"));
                            tmp.setKegiatanNama(hasils.getJSONObject(i).getString("kegiatanNama"));
                            tmp.setJenis(hasils.getJSONObject(i).getString("jenis"));
                            tmp.setCatatan(hasils.getJSONObject(i).getString("catatan"));
                            tmp.setNomor(g_no + i + 1);
                            tmp.setCatatanAtasan(hasils.getJSONObject(i).getString("catatanAtasan"));
                            tmp.setLokasiKoordinat(hasils.getJSONObject(i).getString("lokasiKoordinat"));
                            tmp.setLokasiAlamat(hasils.getJSONObject(i).getString("lokasiAlamat"));
                            tmp.setApproval8HariKebelakangTerkunci(data.getBoolean("isApproval8HariKebelakangTerkunci"));
                            ArrayList<String> stringArray = new ArrayList<String>();
                            if (hasils.getJSONObject(i).getString("file") != null && !hasils.getJSONObject(i).getString("file").isEmpty() &&
                                    !hasils.getJSONObject(i).getString("file").equals("null") && !hasils.getJSONObject(i).getString("file").equals("")) {
                                stringArray.add(hasils.getJSONObject(i).getString("file"));
                            }
                            tmp.setFile(stringArray);
                            mItems.add(tmp);
                        }
                    }
                    else {
                        M.T(getActivity(), obj.getString("message"));
                    }

                    if (getCurrentPage() != 1) {
                        List<AktivitasOsModel> oldItems;
                        if (St.getInstance().getAktif_status().equalsIgnoreCase("0") ||
                                St.getInstance().getAktif_status().equalsIgnoreCase("102") || St.getInstance().getAktif_status().equalsIgnoreCase("100")) {
                            oldItems = aMadapter.getItems();
                        } else {
                            oldItems = mAdapter.getItems();
                        }
                        oldItems.addAll(mItems);
                        if (St.getInstance().getAktif_status().equalsIgnoreCase("0") ||
                                St.getInstance().getAktif_status().equalsIgnoreCase("102") || St.getInstance().getAktif_status().equalsIgnoreCase("100")) {
                            aMadapter.setItems(oldItems);
                        } else {
                            mAdapter.setItems(oldItems);
                        }
                    } else {
                        if (St.getInstance().getAktif_status().equalsIgnoreCase("0") ||
                                St.getInstance().getAktif_status().equalsIgnoreCase("102") || St.getInstance().getAktif_status().equalsIgnoreCase("100")) {
                            aMadapter.setItems(mItems);
                        } else {
                            mAdapter.setItems(mItems);
                        }
                    }
                } catch (Exception e) {
                    M.T(getActivity(), e.getMessage());
                    System.out.println("ERR: " + e.getMessage());
                }
            }

            @Override
            public void failure(RetrofitError error) {
                M.T(getActivity(), getString(R.string.ServerError));
                System.out.println("ERR: " + error.getMessage());
            }
        });
        M.hideLoadingDialog();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.searchBtn) {
            String searchValue = searchField.getText().toString().trim();
            setCurrentSearch(searchValue);
            Integer pos = (searchBulan.getSelectedItemPosition() + 1);
            St.getInstance().setAktif_bulan(pos.toString());
            search(searchValue, 1);
        }
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public String getCurrentSearch() {
        return currentSearch;
    }

    public void setCurrentSearch(String currentSearch) {
        this.currentSearch = currentSearch;
    }

    @Override
    public void onResume(){
        super.onResume();
        String searchValue = searchField.getText().toString().trim();
        setCurrentSearch(searchValue);
        Integer pos = (searchBulan.getSelectedItemPosition() + 1);
        St.getInstance().setAktif_bulan(pos.toString());
        search(searchValue, 1);
    }
}
