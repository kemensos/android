package id.go.kemensos.eperformance.adapters;

import android.view.View;

/**
 * Created by endy on 10/23/2017.
 * Define one interface RecyclerViewClickListener for passing message from adapter to Activity/Fragment
 */
public interface RecyclerViewClickListener {
    public void recyclerViewListClicked(View v, int position);
}
