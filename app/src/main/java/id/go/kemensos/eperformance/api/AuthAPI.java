package id.go.kemensos.eperformance.api;

import id.go.kemensos.eperformance.data.LoginModel;
import id.go.kemensos.eperformance.data.LoginOsModel;
import id.go.kemensos.eperformance.data.ResponseModel;

import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.mime.TypedFile;



public interface AuthAPI {
    @FormUrlEncoded
    @POST("/check.shtml") //obj[action]=login_pegawai
    void login(@Field("obj[action]") String action, @Field("obj[username]") String username,
               @Field("obj[password]") String password,
               @Field("obj[phone]") String phone,
               @Field("obj[device_id]") String device_id,
               @Field("obj[gcm_id]") String gcm_id,
               @Field("obj[token]") String token,
               Callback<LoginModel> response);

    @FormUrlEncoded
    @POST("/nonpnsCheck.shtml") //obj[action]=login_pegawai
    void loginOS(@Field("obj[action]") String action, @Field("obj[username]") String username,
                 @Field("obj[password]") String password,
                 @Field("obj[phone]") String phone,
                 @Field("obj[device_id]") String device_id,
                 @Field("obj[gcm_id]") String gcm_id,
                 @Field("obj[token]") String token,
                 Callback<LoginOsModel> response);

    @Multipart
    @POST("/register")
    void register(@Part("image") TypedFile image,
                  @Part("username") String username,
                  @Part("password") String Password,
                  @Part("email") String email,
                  Callback<ResponseModel> response);
}
