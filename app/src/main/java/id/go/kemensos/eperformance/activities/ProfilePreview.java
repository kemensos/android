package id.go.kemensos.eperformance.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import id.go.kemensos.eperformance.R;
import id.go.kemensos.eperformance.helpers.M;

public class ProfilePreview extends Activity implements View.OnClickListener {
    public TextView userProfileName,
            userTotalFollowers,
            userTotalPosts,
            userProfileAddress,
            userProfileJob,
            userPostsBtn,
            userTelp, userEmail;
    public ImageView userProfilePicture, userProfileCover;
    public LinearLayout actionProfileArea;
    public int userID = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (M.getToken(this) == null) {
            Intent mIntent = new Intent(this, LoginActivity.class);
            startActivity(mIntent);
            finish();
        }else {
            setContentView(R.layout.activity_profile_preview);
            initializeView();
            if (getIntent().hasExtra("userID")) {
                userID = getIntent().getExtras().getInt("userID");
            }
            getUser();
        }

        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    private void initializeView() {
        userProfileName = (TextView) findViewById(R.id.userProfileName);
        userProfilePicture = (ImageView) findViewById(R.id.userProfilePicture);
        userTotalFollowers = (TextView) findViewById(R.id.userTotalFollowers);
        userTotalPosts = (TextView) findViewById(R.id.userTotalPosts);
        actionProfileArea = (LinearLayout) findViewById(R.id.actionProfileArea);
        userProfileAddress = (TextView) findViewById(R.id.userProfileAddress);
        userProfileJob = (TextView) findViewById(R.id.userProfileJob);
        userProfileCover = (ImageView) findViewById(R.id.userProfileCover);
        userPostsBtn = (TextView) findViewById(R.id.userPostsBtn);
        userTelp = (TextView) findViewById(R.id.userTelp);
        userEmail = (TextView) findViewById(R.id.userEmail);
        userPostsBtn.setOnClickListener(this);
    }

    private void getUser() {
        M.L("get-user");

    }

    private void updateView() {

        /*
        if (user.isMine()) {
            actionProfileArea.setVisibility(View.GONE);
        } else {
            actionProfileArea.setVisibility(View.VISIBLE);
        }
        if (user.isFollowed()) {
            followProfileBtn.setText(getString(R.string.UnFollow));
            contactProfileBtn.setVisibility(View.VISIBLE);
        } else {
            followProfileBtn.setText(getString(R.string.Follow));
            contactProfileBtn.setVisibility(View.GONE);
        }
        */

        /*if (user.getName() != null) {
            userProfileName.setText(user.getName());
        } else {
            userProfileName.setText(user.getUsername());
        }
        if (user.getAddress() != null) {
            userProfileAddress.setVisibility(View.VISIBLE);
            userProfileAddress.setText(user.getAddress());
        } else {
            userProfileAddress.setVisibility(View.GONE);
        }
        if (user.getJob() != null){
            userProfileJob.setVisibility(View.VISIBLE);
            userProfileJob.setText(user.getJob());
        } else{
            userProfileJob.setVisibility(View.GONE);
        }
        userTelp.setText(" " + user.getEmp_mobile());
        userTelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String phone_no = userTelp.getText().toString().trim().replaceAll("-", "");
                Intent callIntent = new Intent(Intent.ACTION_DIAL);
                callIntent.setData(Uri.parse("tel:" + phone_no));
                callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(callIntent);
            }
        });
        userEmail.setText(Html.fromHtml(" <a href=\"mailto:" + user.getEmail().trim() + "\">" + user.getEmail().trim() + "</a>"));
        userEmail.setMovementMethod(LinkMovementMethod.getInstance());
        Picasso.with(this)
                //.load(AppConst.IMAGE_PROFILE_URL + user.getPicture())
                .load(user.getPicture())
                .transform(new CropSquareTransformation())
                .placeholder(R.drawable.image_holder)
                .error(R.drawable.image_holder)
                .into(userProfilePicture);
        if (user.getCover() != null) {
            Picasso.with(this)
                    //.load(AppConst.IMAGE_COVER_URL + user.getCover())
                    .load(user.getPicture())
                    .placeholder(R.drawable.header)
                    .error(R.drawable.header)
                    .into(userProfileCover);
        } else {
            Picasso.with(this)
                    .load(R.drawable.header)
                    .into(userProfileCover);
        }
        //userTotalFollowers.setText(user.getTotalFollowers() + "");
        //userTotalPosts.setText(user.getTotalPosts() + "");
        userPostsBtn.setText("Total Post ("+ user.getTotalPosts() + ")");
        M.hideLoadingDialog();*/
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.userPostsBtn:

                break;
        }
    }
}
