package id.go.kemensos.eperformance.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.TextView;

import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import id.go.kemensos.eperformance.GPSTracker;
import id.go.kemensos.eperformance.PermissionManager;
import id.go.kemensos.eperformance.R;
import id.go.kemensos.eperformance.api.APIService;
import id.go.kemensos.eperformance.api.AuthAPI;
import id.go.kemensos.eperformance.api.GlobalAPI;
import id.go.kemensos.eperformance.app.AppConst;
import id.go.kemensos.eperformance.data.LoginModel;
import id.go.kemensos.eperformance.data.LoginOsModel;
import id.go.kemensos.eperformance.data.St;
import id.go.kemensos.eperformance.helpers.M;
import id.go.kemensos.eperformance.helpers.Sess;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Header;
import retrofit.client.Response;

/**
 * Created by enith on 10/9/2015.
 */
public class SplashScreen extends Activity {
    private static int SPLASH_TIME_OUT = 500;//2000;
    TextView txtInfo;
    AsyncTask<Void, Void, Void> mRegisterTask;
    final Context context = this;
    int currentVersion = 0, latestVersion = 0;
    private boolean flag = false;
    private final int MY_PERMISSIONS_REQUEST_CODE = 1;
    PermissionManager permissions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);

        setContentView(R.layout.splashscreen);

        checkPermission(this);
    }

    public void startApplication() {
        //check version app
        try {
            String version = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
            currentVersion = Integer.parseInt(version.replace(".", ""));
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        new GetVersionCode().execute();
    }

    public void doInSplashScreen() {
        //If the versions are not the same
//        if (currentVersion != latestVersion) {
//            // custom dialog
//            final Dialog dialog = new Dialog(context, R.style.PauseDialog);
//            dialog.setTitle(R.string.perbarui);
//            dialog.setContentView(R.layout.custom_dialog_update);
//            Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
//            // if button is clicked, close the custom dialog
//            dialogButton.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    //Click button action
//                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=id.go.kotasurabaya.eperformance")));
//                    dialog.dismiss();
//                }
//            });
//            dialog.setCancelable(false);
//            dialog.show();
//
//        } else {
            //check status gps
            GPSTracker gps = new GPSTracker(this);
            if (!gps.canGetLocation()) {
                gps.showSettingsAlert();
                flag = true;
            } else {

                St.getInstance().setToken("tes1");
                St.getInstance().setDevice_id(Settings.Secure.getString(getApplicationContext().getContentResolver(),
                        Settings.Secure.ANDROID_ID));
//                String phone = ((TelephonyManager) getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE)).getLine1Number();
                String phone = "+1234567890";
                St.getInstance().setPhone(phone);

                St.getInstance().setSplashScreen(this);


                Sess sess = new Sess(this);

                if (sess.ambil("reqid").equals("") || sess.ambil("reqid").isEmpty() || sess.ambil("reqid").equals(null)) {
                    final String regId = FirebaseInstanceId.getInstance().getToken();
                    Log.d(AppConst.TAG,"Token FCM = " + regId);
                    if (!regId.equals("") || !regId.isEmpty() || !regId.equals(null)) {
                        try {
                            sess.simpen("reqid", FirebaseInstanceId.getInstance().getToken("1088461656965", "FCM"));
                            lanjut(SPLASH_TIME_OUT * 5);
                        } catch (IOException e) {
                            e.printStackTrace();
                            M.T(SplashScreen.this, e.getMessage());
                            return;
                        }
                    }
                } else {
                    lanjut(SPLASH_TIME_OUT * 5);
                }

//                if (regId.equals("") || regId.isEmpty() || regId.equals(null)) {
//                    GCMRegistrar.register(this, GCommonUtilities.SENDER_ID);
//                    lanjut(SPLASH_TIME_OUT * 5);
//                } else {
//                    if (GCMRegistrar.isRegisteredOnServer(this)) {
//                        Toast.makeText(getApplicationContext(), "registered...", Toast.LENGTH_LONG).show();
//                        sess.simpen("reqid", GCMRegistrar.getRegistrationId(getApplicationContext()));
//                        Log.d(AppConst.TAG, GCMRegistrar.getRegistrationId(getApplicationContext()));
//                        lanjut(SPLASH_TIME_OUT);
//                    } else {
//                        final Context context = this;
//                        mRegisterTask = new AsyncTask<Void, Void, Void>() {
//                            @Override
//                            protected Void doInBackground(Void... params) {
//                                Log.d(AppConst.TAG, "try-reg-again:" + regId);
//                                return null;
//                            }
//
//                            @Override
//                            protected void onPostExecute(Void result) {
//                                String regId = GCMRegistrar.getRegistrationId(SplashScreen.this);
//                                Sess sess = new Sess(SplashScreen.this);
//                                sess.simpen("reqid", regId);
//                                Log.d(AppConst.TAG, "reqid=" + regId);
//                                mRegisterTask = null;
//                                lanjut(SPLASH_TIME_OUT);
//                            }
//                        };
//                        mRegisterTask.execute(null, null, null);
//                    }
//                }

                //lanjut(); //mode timer lambat

            }
//        }
    }

    void lanjut(int timer) {
        new Handler().postDelayed(new Runnable() {
            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */
            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

                String name = preferences.getString("name", null);
                String id_user = preferences.getString("id_user", null);

                String e = preferences.getString("email", null);
                String p = preferences.getString("p", null);

                Intent i;

                /*if(name!=null||id_user!=null){
                    i = new Intent(SplashScreen.this, Login2Activity.class);
                }else{
                    i = new Intent(SplashScreen.this, LoginActivity.class);
                }
                startActivity(i);*/

                Intent mIntent;
                if (M.getToken(SplashScreen.this) == null) {
                    mIntent = new Intent(SplashScreen.this, LoginActivity.class);
                    startActivity(mIntent);
                    finish();
                } else {
                    //
                    //mIntent = new Intent(SplashScreen.this, MainActivity.class);
                    //startActivity(mIntent);
                    //
                    tri_again();
                }


                // close this activity
                //finish();
            }
        }, timer);
    }

    public void tri_again() {
        Sess s = new Sess(getApplicationContext());
        String e = s.ambil("email");
        String p = s.ambil("p");
        String status = s.ambil("sts");
        //M.showLoadingDialog(this);
        AuthAPI mAuthenticationAPI = APIService.createService(AuthAPI.class, M.getToken(SplashScreen.this));

        if (status.equalsIgnoreCase("os")) {
            mAuthenticationAPI.loginOS("loginPegawai", e, p, St.getInstance().getPhone(), St.getInstance().getDevice_id(),
                s.ambil("reqid"), St.getInstance().getToken(), new Callback<LoginOsModel>() {
                @Override
                public void success(LoginOsModel loginModel, retrofit.client.Response response) {
                    //M.hideLoadingDialog();
                    for (Header header : response.getHeaders()) {
                        if (null != header.getName() && header.getName().equals("Set-Cookie")) {
                            Log.d(AppConst.TAG, header.getName() + " = " + header.getValue());
                            for (String possibleSessionCookie : header.getValue().split(";")) {
                                if (possibleSessionCookie.startsWith("BAMBANGTRIBASUKI") && possibleSessionCookie.contains("=")) {
                                    String session = possibleSessionCookie.split("=")[1];
                                    Log.d(AppConst.TAG, "k=" + session);
                                    St.getInstance().setKoplak(session);
                                }
                            }
                        }
                    }
                    if (loginModel == null) {
                        M.T(SplashScreen.this, "invalid respone");
                        return;
                    }
                    if (loginModel.getData() == null) {
                        M.T(SplashScreen.this, "invalid respone");
                        return;
                    }
                    if (loginModel.isSuccess()) {
                        M.setToken(loginModel.getData().getToken(), SplashScreen.this);
                        M.setID(loginModel.getData().getPegawai_id(), SplashScreen.this);
                        St.getInstance().setLoginOs(loginModel);
                        Log.d(this.getClass().getSimpleName(), "atasan=" + loginModel.getData().getAtasan());
                        //
                        Intent mIntent = new Intent(SplashScreen.this, MainActivity2.class);
                        startActivity(mIntent);
                        finish();
                    } else {
                        M.logOut(SplashScreen.this);
                        Intent mIntent = new Intent(SplashScreen.this, LoginActivity.class);
                        startActivity(mIntent);
                        M.T(SplashScreen.this, loginModel.getMessage());
                        finish();
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    //M.hideLoadingDialog();
                    M.T(SplashScreen.this, getString(R.string.ServerError));
                    System.out.println("ERR: " + error.getMessage());
                }
            });
        } else {
            mAuthenticationAPI.login("loginPegawai", e, p, St.getInstance().getPhone(), St.getInstance().getDevice_id(),
                    s.ambil("reqid"), St.getInstance().getToken(), new Callback<LoginModel>() {
                @Override
                public void success(LoginModel loginModel, retrofit.client.Response response) {
                    //M.hideLoadingDialog();
                    for (Header header : response.getHeaders()) {
                        if (null != header.getName() && header.getName().equals("Set-Cookie")) {
                            Log.d(AppConst.TAG, header.getName() + " = " + header.getValue());
                            for (String possibleSessionCookie : header.getValue().split(";")) {
                                if (possibleSessionCookie.startsWith("BAMBANGTRIBASUKI") && possibleSessionCookie.contains("=")) {
                                    String session = possibleSessionCookie.split("=")[1];
                                    Log.d(AppConst.TAG, "k=" + session);
                                    St.getInstance().setKoplak(session);
                                }
                            }
                        }
                    }
                    if (loginModel == null) {
                        M.T(SplashScreen.this, "invalid respone");
                        return;
                    }
                    if (loginModel.getData() == null) {
                        M.T(SplashScreen.this, "invalid respone");
                        return;
                    }
                    if (loginModel.getData().getSuccess() == 1) {
                        M.setToken(loginModel.getData().getToken(), SplashScreen.this);
                        M.setID(Integer.parseInt(loginModel.getData().getPegawaiId()), SplashScreen.this);
                        St.getInstance().setLogin(loginModel);
                        Log.d(this.getClass().getSimpleName(), "atasan=" + loginModel.getData().getAtasan());
                        //
                        Intent mIntent = new Intent(SplashScreen.this, MainActivity.class);
                        startActivity(mIntent);
                        finish();
                    } else {
                        M.logOut(SplashScreen.this);
                        Intent mIntent = new Intent(SplashScreen.this, LoginActivity.class);
                        startActivity(mIntent);
                        M.T(SplashScreen.this, loginModel.getData().getMsg());
                        finish();
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    //M.hideLoadingDialog();
                    M.T(SplashScreen.this, getString(R.string.ServerError));
                    System.out.println("ERR: " + error.getMessage());
                }
            });
        }
        /*LoginModel loginModel = new LoginModel();
        St.getInstance().setLogin(loginModel);
        Intent mIntent = new Intent(SplashScreen.this, MainActivity.class);
        startActivity(mIntent);
        finish();*/
    }

    public void logut() {
        M.logOut(this);
        Intent mIntent = new Intent(this, LoginActivity.class);
        startActivity(mIntent);
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (flag) {
            startActivity(new Intent(this, SplashScreen.class));
            finish();
        }
    }

//    @Override
//    public boolean dispatchTouchEvent(MotionEvent ev) {
//        tri_again();
//        return super.dispatchTouchEvent(ev);
//    }

    private void checkPermission(Context context) {
        permissions = new PermissionManager();
        if (permissions.checkPermissionLocation(this) && permissions.checkPermissionCamera(this)
            && permissions.checkPermissionStorage(this) && permissions.checkPermissionPhoneState(this)) {
            startApplication();
        } else {
            ArrayList<String> list = new ArrayList<String>();
            if (!permissions.checkPermissionLocation(this)) {
                list.add(Manifest.permission.ACCESS_FINE_LOCATION);
            }
            if (!permissions.checkPermissionCamera(this)) {
                list.add(Manifest.permission.CAMERA);
            }
            if (!permissions.checkPermissionStorage(this)) {
                list.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            }
            if (!permissions.checkPermissionPhoneState(this)) {
                list.add(Manifest.permission.READ_PHONE_STATE);
            }
            String[] permissionArr = new String[list.size()];
            permissionArr = list.toArray(permissionArr);
            ActivityCompat.requestPermissions(this, permissionArr, MY_PERMISSIONS_REQUEST_CODE);
        }
    }

    private void cek_version_webservice(String versionName) {
        M.showLoadingDialog(SplashScreen.this);
        GlobalAPI api = APIService.createService(GlobalAPI.class, M.getToken(SplashScreen.this));
        api.cek_version("doValidateVersion".toString(), Integer.parseInt(versionName.replace(".", "")),
                new Callback<Response>() {
                    @Override
                    public void success(Response resultx, Response response) {
                        M.hideLoadingDialog();
                        //
                        BufferedReader reader = null;
                        StringBuilder sb = new StringBuilder();
                        try {
                            reader = new BufferedReader(new InputStreamReader(resultx.getBody().in()));
                            String line;
                            try {
                                while ((line = reader.readLine()) != null) {
                                    sb.append(line);
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        String res = sb.toString();
                        try {
                            JSONObject obj = new JSONObject(res);
                            JSONObject data = obj.getJSONObject("data");
                            Integer success = data.getInt("success");
                            String msg = data.getString("msg");
                            M.T(SplashScreen.this, msg);
                            if (success == 1) {

                            } else {

                            }
                        } catch (Exception e) {
                            M.T(SplashScreen.this, e.getMessage());
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        M.hideLoadingDialog();
                        M.T(SplashScreen.this, getString(R.string.ServerError));
                        System.out.println("ERR: " + error.getMessage());
                    }
                });
    }

    public class GetVersionCode extends AsyncTask<Void, String, String> {

        @Override

        protected String doInBackground(Void... voids) {

            String newVersion = null;

            try {
                //Document document = Jsoup.connect("https://play.google.com/store/apps/details?id=" + SplashScreen.this.getPackageName() + "&hl=en")
                Document document = Jsoup.connect("https://play.google.com/store/apps/details?id=id.go.kemensos.eperformance" + "&hl=en")
                        .timeout(30000)
                        .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                        .referrer("http://www.google.com")
                        .get();
                if (document != null) {
                    Elements element = document.getElementsContainingOwnText("Current Version");
                    for (Element ele : element) {
                        if (ele.siblingElements() != null) {
                            Elements sibElemets = ele.siblingElements();
                            for (Element sibElemet : sibElemets) {
                                newVersion = sibElemet.text();
                            }
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return newVersion;

        }


        @Override
        protected void onPostExecute(String onlineVersion) {
            super.onPostExecute(onlineVersion);
            if (onlineVersion != null && !onlineVersion.isEmpty()) {
                // if (Float.valueOf(currentVersion) < Float.valueOf(onlineVersion)) {
                //show anything
                // }
                latestVersion = Integer.parseInt(onlineVersion.replace(".", ""));
                doInSplashScreen();
            }
            Log.d("cek update", "Current version = " + currentVersion + " playstore version = " + latestVersion);

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode != MY_PERMISSIONS_REQUEST_CODE) {
            return;
        }
        boolean isGranted = true;
        for (int result : grantResults) {
            if (result != PackageManager.PERMISSION_GRANTED) {
                isGranted = false;
                break;
            }
        }
        if (isGranted) {
            startApplication();
        } else {
            finish();
            System.exit(0);
        }
    }

}
