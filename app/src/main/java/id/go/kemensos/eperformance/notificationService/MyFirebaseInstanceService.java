package id.go.kemensos.eperformance.notificationService;


import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import id.go.kemensos.eperformance.helpers.Sess;

public class MyFirebaseInstanceService extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh() {
        //super.onTokenRefresh();

        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Sess sess = new Sess(this);
        sess.simpen("reqid", refreshedToken);
        Log.d("MyFirebaseToken", "Refreshed Token: " + refreshedToken);
    }
}
