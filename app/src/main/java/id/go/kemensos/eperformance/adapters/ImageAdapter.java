package id.go.kemensos.eperformance.adapters;

/**
 * Created by Programmer on 4/23/18.
 */

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.File;

import id.go.kemensos.eperformance.R;


public class ImageAdapter extends BaseAdapter {

    Context context;
    private final String [] path;

    public ImageAdapter(Context context, String [] path){
        //super(context, R.layout.single_list_app_item, utilsArrayList);
        this.context = context;
        this.path = path;
    }

    @Override
    public int getCount() {
        return path.length;
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        ViewHolder viewHolder;

        final View result;

        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.image_list, parent, false);
            viewHolder.gambar = (ImageView) convertView.findViewById(R.id.imageUpload);
//            Zoomy.Builder builder = new Zoomy.Builder((Activity) context).target(viewHolder.gambar);
//            builder.register();
            viewHolder.file = (TextView) convertView.findViewById(R.id.fileUpload);

            result=convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result=convertView;
        }

        if(path[position].contains("https")) {
            String[] data = path[position].split("\\|");
            if (data[2].toString().contains("image/")) {
                Picasso.get().load(data[3]).into(viewHolder.gambar);
                viewHolder.file.setText("");
            } else {
                viewHolder.gambar.setImageResource(0);
                viewHolder.file.setText(data[1]);
            }
        } else {
            viewHolder.gambar.setImageURI(Uri.fromFile(new File(path[position])));
            viewHolder.file.setText("");
        }

        return convertView;
    }

    private static class ViewHolder {
        TextView file;
        ImageView gambar;
    }

}
