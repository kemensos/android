package id.go.kemensos.eperformance.activities;

import android.content.Context;
import android.content.Intent;

/**
 * Created by user on 3/20/16.
 */
public final class GCommonUtilities {
    public static final String SENDER_ID = "217561172797";
    public static final String TAG = "EPERFORMANCE GCM";
    public static final String DISPLAY_MESSAGE_ACTION =
            "com.pasariklan.kutim.eletter.DISPLAY_MESSAGE";
    public static final String EXTRA_MESSAGE = "message";
    public static void displayMessage(Context context, String message) {
        Intent intent = new Intent(DISPLAY_MESSAGE_ACTION);
        intent.putExtra(EXTRA_MESSAGE, message);
        context.sendBroadcast(intent);
    }

}
