package id.go.kemensos.eperformance.api;

import id.go.kemensos.eperformance.data.ResponseModel;

import retrofit.Callback;
import retrofit.client.Response;
import retrofit.http.Body;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Streaming;
import retrofit.mime.MultipartTypedOutput;


public interface GlobalAPI {

    @FormUrlEncoded
    @POST("/check.shtml")
        //obj[action]=getAktivitasPegawaiFilterByBulanOrStatus
    void do_verify(@Field("obj[action]") String action, @Field("obj[token]") String token,
                   //@Field("obj[aktivitas[][]]") List<Map<String, String>> aktivitas,
                   //@FieldMap() Map<String, String> aktivitas,
                   //@Field("obj[aktivitas[]]") List<KonfirmasiAktivitasModel> aktivitas,
                   //@Field("obj[aktivitas[]]") List<Map<String, String>> aktivitas,
                   @Field("obj[aktivitas]") String aktivitas,
                   @Field("obj[status]") String status,
                   Callback<Response> response);

    @FormUrlEncoded
    @POST("/check.shtml")
        //cek capaian
    void cek_capaian(@Field("obj[action]") String action, @Field("obj[token]") String token,
                     @Field("obj[tanggal]") String tanggal, @Field("obj[pegawai_id]") String pegawai_id,
                     Callback<Response> response);

    @FormUrlEncoded
    @POST("/check.shtml")
        //cek kuncian
    void cek_kuncian(@Field("obj[action]") String action, @Field("obj[token]") String token,
                     @Field("obj[bulan]") Integer bulan, @Field("obj[pegawai_id]") String pegawai_id,
                     Callback<Response> response);

    @FormUrlEncoded
    @POST("/check.shtml")
        //obj[action]=getBawahanLangsung
    void bawahan_lst(@Field("obj[action]") String action, @Field("obj[token]") String token,
                     @Field("obj[pegawai_id]") String pegawai_id, @Field("obj[bulan]") String bulan,
                     Callback<Response> response);

    @FormUrlEncoded
    @POST("/check.shtml")
        //obj[action]=
    void bawahan_aktivitas_lst(@Field("obj[action]") String action, @Field("obj[token]") String token,
                               @Field("obj[pegawai_id]") String pegawai_id,
                               @Field("obj[bulan]") String bulan,
                               @Field("obj[status]") String status,
                               @Field("obj[page]") int page,
                               @Field("obj[jml_per_page]") int jml_per_page,
                               Callback<Response> response);

    @FormUrlEncoded
    @POST("/check.shtml")
        //obj[action]=getAktivitasPegawaiFilterByBulanOrStatus
    void aktivitas_lst(@Field("obj[action]") String action, @Field("obj[token]") String token,
                       @Field("obj[pegawai_id]") String pegawai_id,
                       @Field("obj[bulan]") String bulan,
                       @Field("obj[page]") String page,
                       @Field("obj[jml_per_page]") String jml_per_page,
                       @Field("obj[status]") String status,
                       Callback<Response> response);

    @FormUrlEncoded
    @POST("/nonpnsCheck.shtml")
        //obj[action]=getAktivitasPegawaiByBulanAndStatus
    void aktivitas_os(@Field("obj[action]") String action, @Field("obj[token]") String token,
                       @Field("obj[pegawai_id]") int pegawai_id,
                       @Field("obj[bulan]") String bulan,
                       @Field("obj[status]") String status,
                       Callback<Response> response);

    @FormUrlEncoded
    @POST("/nonpnsCheck.shtml")
        //obj[action]=validasiEntriAktivitasByTanggalAktivitas
    void master_os(@Field("obj[action]") String action,
                   @Field("obj[token]") String token,
                   @Field("obj[tanggal]") String bulan,
                   @Field("obj[pegawai_id]") int pegawai_id,
                   Callback<Response> response);

    @FormUrlEncoded
    @POST("/check.shtml")
    void get_kegiatan(@Field("obj[action]") String action, @Field("obj[token]") String token,
                      @Field("obj[pegawai_id]") String pegawai_id,
                      @Field("obj[bulan]") String bulan,
                      Callback<Response> response);

    @FormUrlEncoded
    @POST("/check.shtml")
    void get_master_aktivitas(@Field("obj[action]") String action, @Field("obj[token]") String token,
                              @Field("obj[pegawai_id]") String pegawai_id,
                              @Field("obj[bulan]") String bulan,
                              Callback<Response> response);

    @FormUrlEncoded
    @POST("/check.shtml")
    void get_duplikasi(@Field("obj[action]") String action, @Field("obj[token]") String token,
                       @Field("obj[pegawai_id]") String pegawai_id,
                       Callback<Response> response);

    @FormUrlEncoded
    @POST("/check.shtml")
    void get_badge(@Field("obj[action]") String action, @Field("obj[token]") String token,
                   @Field("obj[pegawai_id]") String pegawai_id,
                   Callback<Response> response);

    @FormUrlEncoded
    @POST("/check.shtml")
    void get_data(@Field("obj[action]") String action, @Field("obj[token]") String token,
                  @Field("obj[aktivitas_id]") String aktivitas_id,
                  Callback<Response> response);

    @FormUrlEncoded
    @POST("/check.shtml")
    void cek_version(@Field("obj[action]") String action, @Field("obj[version]") int version,
                     Callback<Response> response);

    @FormUrlEncoded
    @POST("/check.shtml")
    void get_master_aktivitas_struktural(@Field("obj[action]") String action, @Field("obj[token]") String token,
                                         Callback<Response> response);

    @POST("/check.shtml")
    void aktivitas_simpan(@Body MultipartTypedOutput data, Callback<Response> response);

    @POST("/nonpnsCheck.shtml")
    void aktivitas_os_simpan(@Body MultipartTypedOutput data, Callback<Response> response);

    @FormUrlEncoded
    @POST("/check.shtml")
    void aktivitas_struktural_simpan(@Field("obj[action]") String action, @Field("obj[token]") String token,
                                     @Field("obj[pegawai_id]") String pegawai_id,
                                     @Field("obj[id]") String id,
                                     @Field("obj[tanggal]") String tanggal,
                                     @Field("obj[aktivitas_string]") String aktivitas_string,
                                     @Field("obj[menit_waktu_pengerjaan]") String menit_waktu_pengerjaan,
                                     @Field("obj[jumlah_output]") String jumlah_output,
                                     @Field("obj[satuan_output]") String satuan_output,
                                     @Field("obj[jenis_waktu_pengerjaan]") int jenis_waktu_pengerjaan,
                                     @Field("obj[kegiatan_id]") String kegiatan_id,
                                     @Field("obj[jenis]") String jenis,
                                     @Field("obj[catatan]") String catatan,
                                     Callback<Response> response);

    @FormUrlEncoded
    @POST("/check.shtml")
    void hapus_file(@Field("obj[action]") String action, @Field("obj[token]") String token,
                    @Field("obj[file_id]") String file_id,
                    Callback<Response> response);

    @FormUrlEncoded
    @POST("/nonpnsCheck.shtml")
    void hapus_file_os(@Field("obj[action]") String action, @Field("obj[token]") String token,
                    @Field("obj[file_id]") String file_id,
                    Callback<Response> response);

    @FormUrlEncoded
    @POST("/check.shtml")
    void hapus_aktivitas(@Field("obj[action]") String action, @Field("obj[token]") String token,
                         @Field("obj[id]") String aktivitas_id,
                         Callback<Response> response);

    @FormUrlEncoded
    @POST("/nonpnsCheck.shtml")
    void hapus_aktivitas_os(@Field("obj[action]") String action, @Field("obj[token]") String token,
                         @Field("obj[id]") int aktivitas_id,
                         Callback<Response> response);

    @Multipart()
    @POST("/map/get")
    void getMapImageUrl(@Field("place_name") String placeName, Callback<ResponseModel> MapInfo);

    @GET("/image/large/{imageid}")
    @Streaming
    void getImage(@Path("imageid") String imageID, Callback<Response> response);

    @FormUrlEncoded
    @POST("/users/regid/")
    void updateRegID(@Field("regID") String regID, Callback<ResponseModel> response);

    @GET("/terms")
    void getTerms(Callback<ResponseModel> responseModel);

    @FormUrlEncoded
    @POST("/check.shtml")
    void get_target_realisasi(@Field("obj[action]") String action, @Field("obj[token]") String token,
                      @Field("obj[pegawai_id]") String pegawai_id,
                      @Field("obj[bulan]") String bulan,
                      Callback<Response> response);

    @FormUrlEncoded
    @POST("/nonpnsCheck.shtml")
    void get_target_realisasi_os(@Field("obj[action]") String action, @Field("obj[token]") String token,
                              @Field("obj[pegawai_id]") int pegawai_id,
                              @Field("obj[bulan]") String bulan,
                              Callback<Response> response);
}