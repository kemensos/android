package id.go.kemensos.eperformance.data;

/**
 * Created by BeN CheRiF on 13/03/2015.
 */
public class DrawerItem {
    public int iconId;
    public String title;
}
